cmake_minimum_required(VERSION 3.5)
project(probe_design)
set(PROJECT_VENDOR "Carl Barton")
set(PROJECT_CONTACT "carl@ebi.ac.uk")
set(PROJECT_DESCRIPTION "Chromotrace")
include(ExternalProject)
find_package(Git)
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
set(CMAKE_BUILD_TYPE "Release")

message(STATUS "Checking if a git repo at ${PROJECT_SOURCE_DIR}")


if(GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
    # Update submodules as needed
    option(GIT_SUBMODULE "Check submodules during build" ON)
    if(GIT_SUBMODULE)
        message(STATUS "Submodule update")
	    execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
	                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	                RESULT_VARIABLE GIT_SUBMOD_RESULT)
	if(EXISTS "${PROJECT_SOURCE_DIR}/external/lib/libsdsl.a")
	    message(STATUS "SDSL-lite library found.")
	else()
            execute_process(COMMAND ${PROJECT_SOURCE_DIR}/sdsl-lite/install.sh ${PROJECT_SOURCE_DIR}/external/)
        endif()
	if(NOT GIT_SUBMOD_RESULT EQUAL "0")
	    message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
	endif()
    endif()
endif()		

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    # require at least gcc 4.7
    if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.7)#Require C++11 support
        message(FATAL_ERROR "GNU version must be at least 4.7!")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -funroll-loops") #Include assert statements
    endif()
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    # require at least clang 3.4
    if (CMAKE_CXX_COMPILER_VERSION VERSION_EQUAL 3.4)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -O3 -funroll-loops") #Include assert statements
    elseif(CMAKE_CXX_COMPILER_VERSION VERSION_LESS 3.5)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -funroll-loops") #Include assert statements
    else()
        message(FATAL_ERROR "Clang version must be at least 3.4!")
    endif()
endif()



find_package(Doxygen)
message(STATUS "Try to find Doxygen for documentation generation.")

if(NOT DOXYGEN_FOUND)
	message(WARNING "Doxygen not found - Documentation will be unavaliable.")
endif()

if(DOXYGEN_FOUND)
	message(STATUS "Doxygen found - Documentation will be built.")

    add_custom_target (doc ALL
            ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
endif (DOXYGEN_FOUND)

set(SOURCE_FILES src/nuc_cruc_santa_lucia.cpp src/circle_buffer.h src/nuc_cruc_anchor.cpp src/nuc_cruc_output.cpp src/nuc_cruc.cpp src/nuc_cruc.h src/probe_design_main.cpp src/probe_design_main.h src/suffix_tree.cpp src/suffix_tree.h src/run_algorithm.cpp src/read_fasta_files.h src/read_fasta_files.cpp src/simple_filters.h src/simple_filters.cpp)

add_executable(probe_design ${SOURCE_FILES})

find_library(SDSL_LIB sdsl
	HINTS ${PROJECT_SOURCE_DIR}/external/lib/)
find_path(SDSL_INC sdsl
	HINTS ${PROJECT_SOURCE_DIR}/external/include/)
message(STATUS ${SDSL_LIB})
message(STATUS ${SDSL_INC})

find_library(DIV_SUF_LIB divsufsort
	HINTS ${PROJECT_SOURCE_DIR}/external/lib/)

message(STATUS ${DIV_SUF_LIB})

find_library(DIV_SUF64 divsufsort64
	HINTS ${PROJECT_SOURCE_DIR}/external/lib/)
message(STATUS ${DIV_SUF64})

add_library(sdsl STATIC IMPORTED)
set_target_properties(sdsl PROPERTIES IMPORTED_LOCATION ${SDSL_LIB})
add_library(divsufsort STATIC IMPORTED)
set_target_properties(divsufsort PROPERTIES IMPORTED_LOCATION ${DIV_SUF_LIB})
add_library(divsufsort64 STATIC IMPORTED)
set_target_properties(divsufsort64 PROPERTIES IMPORTED_LOCATION ${DIV_SUF64})

target_link_libraries(probe_design PUBLIC ${SDSL_LIB} ${DIV_SUF_LIB} ${DIV_SUF64} Threads::Threads)
target_include_directories(probe_design PUBLIC ${SDSL_INC})
#add_subdirectory(google_tests)
add_subdirectory(build_suffix_tree)
