#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "probe_design_main.h"

/// A simple method to run the main algorithm
/// \param 
/// \return
int main(int argc, char *argv[]){

    std::cout << "Trying to run probe design with melting temperature"<< std::endl;
    switch(argc){
        case 9:
            probe_design_main(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]), std::stoi(argv[4]), std::stoi(argv[5]), std::stoi(argv[6]), std::string(argv[7]), std::string(argv[8]));
            break;
        case 8:
            probe_design_main(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]), std::stoi(argv[4]), std::stoi(argv[5]), std::stoi(argv[6]), std::string(argv[7]));
            break;
        case 7:
            probe_design_main(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]), std::stoi(argv[4]), std::stoi(argv[5]), std::stoi(argv[6]));
            break;
        case 6:
            probe_design_main(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]), std::stoi(argv[4]), std::stoi(argv[5]));
            break;
        case 5:
            probe_design_main(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]), std::stoi(argv[4]));
            break;
        case 4:
            probe_design_main(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]));
            break;
        default:
            std::cout << "Check arguments, probe_design requires between 3 and 8 arguments. Check README for more details on how to call the program with more than 3 arguments. Below is the simplest way to call probe_design" << std::endl;
            std::cout << "probe_design <path to query sequence file> <path to genome sequence> <path to genome index> " << std::endl;
            std::cout << "probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst " << std::endl;
    }
    return 0;
}
