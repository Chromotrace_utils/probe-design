// Copyright (c), EMBL, 2017
// Created by carl on 30/09/16.
//
#include <iostream>
#include "suffix_tree.h"
#include <algorithm>
/// Method to return a sibling of the provided node in the suffix tree. A wrapper script for the appropriate sdsl method.
/// \param node is a valid node in the suffix tree we want to return a sibling of.
/// \return A sibling of node in the suffix tree.
node_type suffix_tree::sibling(const node_type node)  const {
    return cst.sibling(node);
}

/// Return the left most leaf of a valid node in the suffix tree.
/// \param node A valid node in the suffix tree that we want to return the left most leaf of.
/// \return The left most leaf of the subtree rooted at node in the suffix tree.
node_type suffix_tree::leftmost_leaf(const node_type node)  const {
    return cst.leftmost_leaf(node);
}

/// Return the right most leaf of the subtree rooted at a valid node in the suffix tree.
/// \param node A valid node in the suffix tree that we want to return the right most leaf of
/// \return The rightmost leaf of the subtree rooted at node in the suffix tree.
node_type suffix_tree::rightmost_leaf(const node_type node) const {
    return cst.rightmost_leaf(node);
}

/// Calculates the index of the leftmost leaf in the corresponding suffix array
/// \param node  A valid node of the suffix tree.
/// \return The index of the leftmost leaf in the corresponding suffix array.
size_type suffix_tree::lb(const node_type node) const {
    return cst.lb(node);
}

///  Calculates the index of the rightmost leaf in the corresponding suffix array.
/// \param node A valid node of the suffix tree.
/// \return The index of the rightmost leaf in the corresponding suffix array.
size_type suffix_tree::rb(const node_type node) const {
    return cst.rb(node);
}

/// Return the i-th leaf (1-based from left to right) of the suffix tree.
/// \param node 1-based position of the leaf.
/// \return The i-th leave.
node_type suffix_tree::select_leaf(size_type node) const {
    return cst.select_leaf(node);
}

/// Compute the suffix number of a leaf node v.
/// \param node A valid leaf node of the suffix tree.
/// \return The suffix array value corresponding to the leaf node v.
size_type suffix_tree::sn(node_type node) const {
    return cst.sn(node);
}

/// Get the number of nodes of the suffix tree.
/// \return The number of nodes of the suffix tree.
size_type suffix_tree::nodes() const {
    return cst.nodes();
}

/// Number of leaves in the suffix tree.
/// \return Number of leaves in the suffix tree.
size_type suffix_tree::size(node_type node) const {
    return cst.size(node);
}

/// Return if the given position of the underlying string is a valid poition
/// \param position a number representing an index of the underlying string
/// \return if position is a valid index of the underlying string
bool suffix_tree::valid_pos(unsigned long position) const {
    return position>0 && position<get_text_length();
}

/// Method to return a specified suffix from the suffix tree up to a given length
/// \param suffix_number is a valid suffix of the text
/// \return the suffix starting at suffix number
std::string suffix_tree::get_suffix(const unsigned long suffix_number, const unsigned long length){
    std::string remaining_suffix;

    assert(suffix_number<text_length);

    auto leaf  = get_suffix_leaf(suffix_number+1);
    //auto limit = std::min(cst.depth(leaf),length);
    auto limit = std::min<unsigned long>(cst.depth(leaf),length);

    for(auto counter=1; counter<=limit; counter++){
        remaining_suffix.push_back(cst.edge(leaf, counter));
    }

    return remaining_suffix;
}

/// Method to return a specified suffix from the suffix tree.
/// \param suffix_number is a valid suffix of the text
/// \return the suffix starting at suffix number
std::string suffix_tree::get_suffix(const unsigned long suffix_number){
    return get_suffix(suffix_number, text_length);
}

/// This seems the same as select leaf
/// \param suffix is the number of the suffix leaf requested
/// \return the node representing the leaf for suffix specified
node_type suffix_tree::get_suffix_leaf(const unsigned long suffix) const {
    return cst.select_leaf(suffix);
}

/// Get the parent node of the specified node in the suffix tree
/// \param node the node we want the parent of
/// \return the parent of node
node_type suffix_tree::get_parent(const node_type node) const {
    return cst.parent(node);
}

/// Perform pattern matching for the given pattern starting from the given node
/// \param pattern is the string to search for
/// \param node is a valid node to start pattern matching from
/// \return the length of the matching from the given node
unsigned long suffix_tree::pattern_match(const std::string& pattern, const node_type node) const {

    auto pattern_length = pattern.length();
    node_type current_node       = cst.child(node, pattern.at(0));
    size_type current_depth      = cst.depth(node);

    for(auto index_to_match = current_depth; index_to_match<pattern_length;index_to_match++){

        if(cst.edge(current_node,index_to_match)!=pattern.at(index_to_match-current_depth))
            return index_to_match; //Return length of match

        if(index_to_match==current_depth-1)
            current_node = cst.child(current_node,pattern.at(index_to_match+1));
    }
    return pattern_length;
}

/// Perform pattern matching for the given pattern starting from the root node
/// \param pattern is the string to search for
/// \return the length of the match from the root node
unsigned long suffix_tree::pattern_match(const std::string& pattern) const {//Searching from the root using above
    return pattern_match(pattern, cst.root());
}

/// Determine if the given pattern occurs uniquely in the text with length less than depth.
/// \param pattern is the string to search in the suffix tree
/// \param depth representing the maximum depth
/// \return return the leaf node representing the pattern location
/// if it is unique and root otherwise
node_type suffix_tree::is_unique(const std::string& pattern, const unsigned long depth) const {//Simulate truncated suffix trees

    unsigned long pattern_length = (pattern.length()< depth) ? pattern.length(): depth;

    assert(depth < text_length);//assert the depth is not longer than the text
    assert(depth > 0);

    node_type current_node  = cst.child(cst.root(), pattern.at(0));
    size_type current_depth = cst.depth(current_node);

    for(auto index_to_match = current_depth; index_to_match<pattern_length;index_to_match++){

        if(cst.edge(current_node,current_depth)!=pattern.at(index_to_match-current_depth)) {//Mismatch
            if (cst.size(current_node) == 1) {//Unique?
                return current_node;
            } else {
                return cst.root();
            }
        }

        if(index_to_match==current_depth-1)
            current_node = cst.child(current_node,pattern.at(index_to_match+1));
    }
    return current_node; //If unique return the end of the edge.
}

/// Determine if the given pattern occurs uniquely in the text.
/// \param pattern is the string to search for
/// \return return the leaf node representing the pattern location
/// if it is unique and root otherwise
node_type suffix_tree::is_unique(const std::string& pattern) const {
    return is_unique(pattern, text_length);
}

/// This method should be like matching statistics but instead of just the mismatch condition we have mismatch or the next node is a leaf. This is because
/// we will always be searching for strings which are already in the index.
/// Take a pattern p of length m
/// and compute the longest match
/// for each suffix of p as well as their
/// start position in the text if this
/// match is unique
/// \param pattern is the string to search for in the suffix tree
/// \return std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> that
/// represents the matching_statistics vector and a vector representing their locations
/// TODO: Work out why sometimes this becomes very slow on a specific edge of the suffix tree
std::vector<unsigned long> suffix_tree::kmer_count( std::string pattern, const unsigned long first_depth) const {
    /// Def. msXY(i) := the longest substring of pattern that starts at i and matches someplace in the text.
    /// Algorithm sketch to compute msXY(i)
    ///
    /// Step 1: Compute msXY(1) by querying for X in TY.
    /// The depth of the node where you stop is msXY(1)
    ///
    /// Step 2 for 1<i<n
    /// Follow the suffix link from where the search stopped in the previous step/iteration.
    /// Continue searching for X where you left off in the pattern.
    /// msXY(i) = the depth where the search gets stuck

    node_type current_node  = cst.root();
    node_type next_node     = cst.child(current_node, pattern.at(0));

    long current_depth    = 0;

    auto pattern_length   = pattern.length();
    auto next_match_stats = 0;

    std::vector<unsigned long> kmer_count(pattern_length, 0);

    if(first_depth==0){
        return kmer_count;
    }

    pattern.append("?");

    auto pattern_index=0;
    while(next_match_stats<pattern_length) {
        assert(current_depth >= 0);

        if (is_root(next_node) && is_root(current_node)) {///current character of the pattern doesn't exist in the text at all, skip this character
            kmer_count.at(next_match_stats) = 0;

            ++next_match_stats;
            ++pattern_index;

            next_node     = ((next_match_stats + current_depth)<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();
        } else if (is_root(next_node)) {///current character of the pattern doesn't exist at this node
            /// What to do about mismatch on an edge
            ///

            kmer_count.at(next_match_stats) = cst.size(current_node);
            ++next_match_stats;

            current_node  = cst.sl(current_node);///As current character doesn't match we follow the suffix link.
            current_depth = cst.depth(current_node);
            next_node     = ((next_match_stats + current_depth)<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();

        } else if(current_depth==first_depth || cst.edge(next_node, current_depth + 1) != pattern.at(pattern_index)){
            /// if we match to the end of the string then next_match_stats + target_depth = pattern_length
            /// meaning that the while loop will never find a valid node.

            if(current_depth==cst.depth(current_node)){
                kmer_count.at(next_match_stats) = cst.size(current_node);
            } else {
                kmer_count.at(next_match_stats) = cst.size(next_node);
            }

            ++next_match_stats;
            --current_depth;

            long target_depth = current_depth ;

            current_node  = cst.sl(current_node);///Follow the suffix link for the current node
            current_depth = cst.depth(current_node);
            next_node     = ((next_match_stats + current_depth)<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();//if this is assigned root, current_node may be assigned root immediately. Will never terminate the loop

            while (target_depth - current_depth != 0) {
                ///
                /// This loop puts the pointer to the correct depth in the suffix tree after following the suffix link.
                /// After following the suffix link the current node will be too shallow in the suffix tree because we were not currently at an explicit node.
                /// If this is the case we know there is more of the pattern matched and can jump to this depth without repeating the matching process.
                /// This loop skips nodes until we find the edge we should be on, then sets the current_depth to the target_depth and
                ///

                assert(current_depth>=0);
                if (target_depth - current_depth> 0) {///The next node has a depth less than the target depth. So we don't performing matching and set the current node to the next node
                    current_node  = next_node;
                    current_depth = cst.depth(current_node);

                    next_node     = (next_match_stats + current_depth<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();//Write test script for this

                } else if (target_depth - current_depth< 0) {///count to correct position on edge

                    next_node     = current_node;
                    current_node  = cst.parent(current_node);
                    current_depth = target_depth;

                }
            }
        } else if(cst.edge(next_node, current_depth + 1) == pattern.at(pattern_index)){

            ++pattern_index;
            ++current_depth;

            if(current_depth==cst.depth(next_node) && pattern_index<=pattern_length){

                current_node  = next_node;
                current_depth = cst.depth(current_node);
                next_node     = cst.child(current_node, pattern.at(pattern_index));
            }
        } else {

            kmer_count.at(next_match_stats) = cst.size(next_node);

            ++next_match_stats;

            next_node = cst.root();

        }

    }

    return kmer_count;
}

/// This method should be like matching statistics but instead of just the mismatch condition we have mismatch or the next node is a leaf. This is because
/// we will always be searching for strings which are already in the index.
/// Take a pattern p of length m
/// and compute the longest match
/// for each suffix of p as well as their
/// start position in the text if this
/// match is unique
/// \param pattern is the string to search for in the suffix tree
/// \return std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> that
/// represents the matching_statistics vector and a vector representing their locations
/// TODO: Work out why sometimes this becomes very slow on a specific edge of the suffix tree
std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> suffix_tree::matching_statistics_mark_depth( std::string pattern, const unsigned long first_depth) const {
    /// Def. msXY(i) := the longest substring of pattern that starts at i and matches someplace in the text.
    /// Algorithm sketch to compute msXY(i)
    ///
    /// Step 1: Compute msXY(1) by querying for X in TY.
    /// The depth of the node where you stop is msXY(1)
    ///
    /// Step 2 for 1<i<n
    /// Follow the suffix link from where the search stopped in the previous step/iteration.
    /// Continue searching for X where you left off in the pattern.
    /// msXY(i) = the depth where the search gets stuck

    node_type current_node  = cst.root();
    node_type next_node     = cst.child(current_node, pattern.at(0));

    long current_depth    = 0;

    auto pattern_length   = pattern.length();
    auto next_match_stats = 0;

    std::vector<unsigned long> matching_stats(pattern_length);
    std::vector<unsigned long> marked_nodes(pattern_length);
    std::vector<unsigned long> marked_nodes_depth(pattern_length);

    for(unsigned long init_counter=0;init_counter<pattern_length; ++init_counter){marked_nodes.at(init_counter) = cst.root();} ///Init the array with root node.
    for(unsigned long init_counter=0;init_counter<pattern_length; ++init_counter){marked_nodes_depth.at(init_counter) = 0;} ///Init the array with max possible values.

    if(first_depth==0){
        return std::make_tuple(marked_nodes_depth,marked_nodes);
    }

    pattern.append("?");

    auto pattern_index=0;
    while(next_match_stats<pattern_length) {
        assert(current_depth >= 0);

        if (is_root(next_node) && is_root(current_node)) {///current character of the pattern doesn't exist in the text at all, skip this character
            marked_nodes_depth.at(next_match_stats) = 0;

            ++next_match_stats;
            ++pattern_index;

            next_node     = ((next_match_stats + current_depth)<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();
        } else if (is_root(next_node)) {///current character of the pattern doesn't exist at this node
            /// What to do about mismatch on an edge
            ///

            marked_nodes.at(next_match_stats)       = (cst.is_leaf(next_node)) ? cst.parent(next_node) : next_node;
            marked_nodes_depth.at(next_match_stats) = (cst.is_leaf(next_node)) ? cst.depth(cst.parent(next_node))
                                                                               : current_depth;
            matching_stats.at(next_match_stats) = current_depth;
            ++next_match_stats;

            current_node  = cst.sl(current_node);///As current character doesn't match we follow the suffix link.
            current_depth = cst.depth(current_node);
            next_node     = ((next_match_stats + current_depth)<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();

        } else if(current_depth==first_depth || cst.edge(next_node, current_depth + 1) != pattern.at(pattern_index)){
            /// if we match to the end of the string then next_match_stats + target_depth = pattern_length
            /// meaning that the while loop will never find a valid node.

            if(current_depth==cst.depth(current_node)){
                marked_nodes.at(next_match_stats) = (cst.is_leaf(current_node)) ? cst.parent(current_node) : current_node;
                marked_nodes_depth.at(next_match_stats) = (cst.is_leaf(current_node)) ? cst.depth(cst.parent(current_node)) : cst.depth(current_node);
            } else {
                marked_nodes.at(next_match_stats) = (cst.is_leaf(next_node)) ? current_node : next_node;
                marked_nodes_depth.at(next_match_stats) = (cst.is_leaf(next_node)) ? cst.depth(current_node) : current_depth;
            }

            ++next_match_stats;
            --current_depth;

            long target_depth = current_depth ;

            current_node  = cst.sl(current_node);///Follow the suffix link for the current node
            current_depth = cst.depth(current_node);
            next_node     = ((next_match_stats + current_depth)<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();//if this is assigned root, current_node may be assigned root immediately. Will never terminate the loop

            while (target_depth - current_depth != 0) {
                ///
                /// This loop puts the pointer to the correct depth in the suffix tree after following the suffix link.
                /// After following the suffix link the current node will be too shallow in the suffix tree because we were not currently at an explicit node.
                /// If this is the case we know there is more of the pattern matched and can jump to this depth without repeating the matching process.
                /// This loop skips nodes until we find the edge we should be on, then sets the current_depth to the target_depth and
                ///

                assert(current_depth>=0);
                if (target_depth - current_depth> 0) {///The next node has a depth less than the target depth. So we don't performing matching and set the current node to the next node
                    current_node  = next_node;
                    current_depth = cst.depth(current_node);

                    next_node     = (next_match_stats + current_depth<=pattern_length) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();//Write test script for this

                } else if (target_depth - current_depth< 0) {///count to correct position on edge

                    next_node     = current_node;
                    current_node  = cst.parent(current_node);
                    current_depth = target_depth;

                }
            }
        } else if(cst.edge(next_node, current_depth + 1) == pattern.at(pattern_index)){

            ++pattern_index;
            ++current_depth;

            if(current_depth==cst.depth(next_node) && pattern_index<=pattern_length){

                current_node  = next_node;
                current_depth = cst.depth(current_node);
                next_node     = cst.child(current_node, pattern.at(pattern_index));
            }
        } else {

            marked_nodes.at(next_match_stats) = (cst.is_leaf(next_node)) ? cst.parent(next_node) : next_node;
            marked_nodes_depth.at(next_match_stats) = (cst.is_leaf(next_node)) ? cst.depth(cst.parent(next_node)) : cst.depth(next_node);

            ++next_match_stats;

            next_node = cst.root();

        }

    }

    return std::make_tuple(marked_nodes_depth,marked_nodes);
}

/// Take a pattern p of length m
/// and compute the longest match
/// for each suffix of p as well as their
/// start position in the text if this
/// match is unique
/// \param pattern is the string to search for in the suffix tree.
/// \return std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> that
/// represents the matching_statistics vector and a vector representing their locations
std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> suffix_tree::matching_statistics(std::string pattern) const {
    /// Def. msXY(i) := the longest substring of pattern that starts at i and matches someplace in the text.
    /// Algorithm sketch to compute msXY(i)
    ///
    /// Step 1: Compute msXY(1) by querying for X in TY.
    /// The depth of the node where you stop is msXY(1)
    ///
    /// Step 2 for 1<i<n
    /// Follow the suffix link from where the search stopped in the previous step/iteration.
    /// Continue searching for X where you left off in the pattern.
    /// msXY(i) = the depth where the search gets stuck

    node_type current_node  = cst.root();
    node_type next_node     = cst.child(current_node, pattern.at(0));

    long current_depth    = 0;

    auto pattern_length   = pattern.size();
    auto next_match_stats = 0;

    ///
    /// vectors for storing the matching statistics and the location of the match in the text
    ///
    std::vector<unsigned long> matching_stats(pattern_length);
    std::vector<unsigned long> marked_nodes(pattern_length);

    for(unsigned long init_counter=0;init_counter<pattern_length; ++init_counter){marked_nodes.at(init_counter) = cst.root();} ///Init the array with root node
    for(unsigned long init_counter=0;init_counter<pattern_length; ++init_counter){matching_stats.at(init_counter) = pattern_length - init_counter;} ///Init the array with max possible values.

    auto pattern_index=0;

    pattern.append("?");

    while(pattern_index<pattern_length) {

        assert(current_depth >= 0);
        if ((cst.depth(next_node) == current_depth) || is_root(next_node)){///We are matching at an explicit node
            if (is_root(next_node) && is_root(current_node)) {///current character of the pattern doesn't exist in the text at all, skip this character
                matching_stats.at(next_match_stats) = 0;

                ++next_match_stats;
                ++pattern_index;

                next_node = cst.child(current_node, pattern.at(pattern_index));

            } else if (is_root(next_node)) {///current character of the pattern doesn't exist at this node
                matching_stats.at(next_match_stats) = current_depth;
                marked_nodes.at(next_match_stats)   = current_node;

                ++next_match_stats;

                current_node  = cst.sl(current_node);///As current character doesn't match we follow the suffix link.
                current_depth = cst.depth(current_node);
                next_node     = cst.child(current_node, pattern.at(pattern_index));
            } else {///There is a match, as we are currently at an explicit node we need to get the next node to continue matching
                current_node  = next_node;
                current_depth = cst.depth(current_node);
                next_node     = cst.child(current_node, pattern.at(pattern_index));

                if(!is_root(next_node)){
                    ++pattern_index;
                    ++current_depth;
                }
            }
        } else if (cst.edge(next_node, current_depth + 1) != pattern.at(pattern_index)) {///We are currently performing matching on on an edge and there is a mismatch.

            ///As there is a mismatch we update the matching statistics and location.
            matching_stats.at(next_match_stats) = current_depth;
            marked_nodes.at(next_match_stats)   = next_node;

            --current_depth;
            ++next_match_stats;

            long target_depth = current_depth;

            current_node  = cst.sl(current_node);///Follow the suffix link for the current node
            current_depth = cst.depth(current_node);
            next_node     = cst.child(current_node, pattern.at(next_match_stats + current_depth));

            while (target_depth - current_depth != 0) {
            ///
            /// This loop puts the pointer to the correct depth in the suffix tree after following the suffix link.
            /// After following the suffix link the current node will be too shallow in the suffix tree because we were not currently at an explicit node.
            /// If this is the case we know there is more of the pattern matched and can jump to this depth without repeating the matching process.
            /// This loop skips nodes until we find the edge we should be on, then sets the current_depth to the target_depth and
            ///
                assert(current_depth>=0);
                if (target_depth - current_depth> 0) {///The next node has a depth less than the target depth. So we don't performing matching and set the current node to the next node
                    current_node  = next_node;
                    current_depth = cst.depth(current_node);
                    next_node     = (pattern_length>=next_match_stats + current_depth) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();//Write test script for this
                } else if (target_depth - current_depth< 0) {///count to correct position on edge
                    next_node     = current_node;
                    current_node  = cst.parent(current_node);
                    current_depth = target_depth;
                }
            }
        } else {///We are matching on an edge and the characters being compared match.
            ++current_depth;
            ++pattern_index;
        }
    }

    return std::make_tuple(matching_stats,marked_nodes);
}

/// Determines if the paramater node is the root node
/// \param node valid node in the tree
/// \return true if it is a root and false otherwise

inline bool suffix_tree::is_root(const node_type node) const {
    return cst.root()==node;
}

/// Returns the root node of the suffix tree
/// \return the root node in the suffix tree

node_type suffix_tree::get_root() const{
    return cst.root();
}

/// Return the number of nodes in the suffix tree.
///
/// \return the number of nodes in the suffix tree
size_type suffix_tree::get_size() const{
    return cst.size();
}

/// Get text length
/// \return text length
unsigned long suffix_tree::get_text_length() const{
    return text_length;
}

