//
// Created by Odyss on 17/03/2020.
//

#ifndef PROBE_DESIGN_SIMPLE_FILTERS_H
#define PROBE_DESIGN_SIMPLE_FILTERS_H

#include <string>
#include <vector>

class simple_filters {

public:
    void gc_and_rep_filter(unsigned int probe_length, std::string &pattern, std::vector<bool> &mat,
                           std::vector<bool> &mat_rev);

    void rule_out_big_matches(std::vector<unsigned long> &matching_stats_forward,
                              std::vector<unsigned long> &matching_stats_backward, std::vector<bool> &under_kmer,
                              unsigned int kmer_length, unsigned int kmer_length_2,
                              unsigned int probe_length, std::vector<bool> &mat, std::vector<bool> &mat_rev);

    void exclude_list_filter(unsigned int probe_length, const std::string &exclude_list,
                             std::vector<bool> &mat, std::vector<bool> &mat_rev);

    void kmer_count_filter(unsigned int probe_length, unsigned int kmer_length, std::vector<unsigned long> &kmer_count,
                             std::vector<bool> &mat, std::vector<bool> &mat_rev);
    void kmer_filter(unsigned int probe_length, unsigned int kmer_length,
                std::vector<unsigned long> &kmer_count, std::vector<bool> &mat,
                std::vector<bool> &mat_rev, unsigned int edits);
};


#endif //PROBE_DESIGN_SIMPLE_FILTERS_H
