#include "probe_design_main.h"
#include "simple_filters.h"

/// Verification of potential matches and calculation of tm
/// \param pattern_start start position of the interval of the pattern to be verified.
/// \param pattern_end end position of the interval of the pattern to be verified.
/// \param text_start start position of the interval of the text to be verified.
/// \param text_end end position of the interval of the text to be verified.
/// \param ignore_start is the start position of a sub interval of (pattern_start, pattern_end) to ignore during verification.
/// \param direction an enum Direction type argument that tells the method which of the genome is being checked.
inline void probe_design_main::verify_area_tm_test(const unsigned long pattern_start, const unsigned long pattern_end, const unsigned long text_start, const unsigned long text_end, std::vector<bool> &valid_array, const unsigned long ignore_start, const Direction direction) {

    NucCruc m_melt = NucCruc(NucCruc::SANTA_LUCIA);
    //m_melt.dangle(false, false);
    m_melt.Salt(0.05);
    m_melt.strand(0.0000009);

    if (pattern_end - pattern_start < probe_length)
        return;
    if (text_end - text_start < probe_length)
        return;

    //std::cout << "target: " << std::endl;


    /// Add possible match from text to the buffer
    for (auto const &cur_char : text.substr(text_start, probe_length)) {
        m_melt.push_back_target(cur_char);
        //std::cout << cur_char;
        if (cur_char == 'n') {
            //std::cout <<"break for n" << std::endl;
            return;
        }
    }

    //std::cout << std::endl << "query: " << std::endl;

    if (!(text_start > (ignore_start + pattern_start - probe_length) &&
          text_start < (ignore_start + pattern_start + probe_length))) {
        for (auto const &cur_char : pattern.substr(pattern_start, 2 * probe_length)) {
            //std::cout << cur_char;
            switch (cur_char){
                case 'a' :
                case 'A' :
                    m_melt.push_front_query('t');
                    break;
                case 't' :
                case 'T' :
                    m_melt.push_front_query('a');
                    break;
                case 'c' :
                case 'C' :
                    m_melt.push_front_query('g');
                    break;
                case 'g' :
                case 'G' :
                    m_melt.push_front_query('c');
                    break;
                case 'n' :
                case 'N' :
                    break;
                default:
                    std::cout << "Unknown base in query sequence" << std::endl;
                    break;
            }
        }
    } else{
        //std::cout << "should be ignored" << std::endl;
        return;
    }

    //std::cout << std::endl;

    /// Compute the Smith-Waterman local alignment between two sequences; the query
    /// and the target. As the alignment is local only the high scoring regions will be
    /// found
    float tm = m_melt.approximate_tm_heterodimer();

    unsigned long start_tar;
    unsigned long end_tar;

    unsigned long start_quer;
    unsigned long end_quer;

    std::tie(start_tar, end_tar)   = m_melt.alignment_range_target();
    std::tie(start_quer, end_quer) = m_melt.alignment_range_query();

    //std::cout << "Length: " << end_tar - start_tar << std::endl;
    //std::cout << "TM: " << tm << std::endl;

    for(auto counter = pattern_start + end_quer-(probe_length); counter< pattern_start + start_quer + probe_length; ++counter){
        if (tm > std::get<3>(thread_results.at(pattern_start+start_quer).at(0))) {
            (direction) ? thread_mutex.at(pattern_start+start_quer).lock() : thread_mutex_rev.at(pattern_start+start_quer).lock();
            thread_results.at(pattern_start+start_quer).at(0) = std::make_tuple(pattern_start+start_quer, text_start + start_tar,
                                                                                text_start + end_tar, tm);
            (direction) ? thread_mutex.at(pattern_start+start_quer).unlock() : thread_mutex_rev.at(pattern_start+start_quer).unlock();
        }

        if ((max_on_target_results.at(pattern_start) - tm) < 3) {
            mat.at(pattern_start) = true;
            mat_rev.at(mat.size() - probe_length - pattern_start) = true;
        }
    }
}


/// Verification of potential matches and calculation of tm
/// \param pattern_start start position of the interval of the pattern to be verified.
/// \param pattern_end end position of the interval of the pattern to be verified.
/// \param text_start start position of the interval of the text to be verified.
/// \param text_end end position of the interval of the text to be verified.
/// \param ignore_start is the start position of a sub interval of (pattern_start, pattern_end) to ignore during verification.
/// \param direction an enum Direction type argument that tells the method which of the genome is being checked.
inline void probe_design_main::verify_area_tm(const unsigned long pattern_start, const unsigned long pattern_end, const unsigned long text_start, const unsigned long text_end, std::vector<bool> &valid_array, const unsigned long ignore_start, const Direction direction){

    NucCruc m_melt = NucCruc(NucCruc::SANTA_LUCIA);
    m_melt.dangle(false, false);
    m_melt.Salt(0.05);
    m_melt.strand(0.0000009);

    if(pattern_end-pattern_start<probe_length)
        return;
    if(text_end-text_start<probe_length)
        return;

    unsigned long end_pos_t = text_end-probe_length+1;
    unsigned long end_pos_p = pattern_end-probe_length+1;

    for(unsigned long start_pos_t = text_start; start_pos_t<end_pos_t; ++start_pos_t){//For each start position in the text

        /// Clear text buffer
     	m_melt.clear_target();

     	/// Add possible match from text to the buffer
	    for(auto const & cur_char : text.substr(start_pos_t,probe_length)){
            m_melt.push_back_target(cur_char);
        }

	    /// Each iteration loads the query sequence into the query buffer
	    /// Add the current docking handle
	    /// Save any valid matches that are found
        for(unsigned long start_pos_p = pattern_start; start_pos_p<end_pos_p; ++start_pos_p){//For each pattern
            /// check that the match doesn't overlap with the pattern
            if(!(start_pos_t>(ignore_start+start_pos_p-probe_length) && start_pos_t<(ignore_start+start_pos_p+2*probe_length))) {
                /// Check that the probe being tested hasn't already been marked as invalid.
                if(!valid_array.at(start_pos_p)) {

                    /// Clear the query buffer
                    m_melt.clear_query();

                    /// Add relevent part of the query sequence to query buffer
                    for(auto const & cur_char : pattern.substr(start_pos_p, probe_length)){
                        switch (cur_char){
                            case 'a' :
                            case 'A' :
                                m_melt.push_front_query('t');
                                break;
                            case 't' :
                            case 'T' :
                                m_melt.push_front_query('a');
                                break;
                            case 'c' :
                            case 'C' :
                                m_melt.push_front_query('g');
                                break;
                            case 'g' :
                            case 'G' :
                                m_melt.push_front_query('c');
                                break;
                            case 'n' :
                            case 'N' :
                                break;
                            default:
                                std::cout << "Unknown base in query sequence" << std::endl;
                                break;
                        }
                    }


       	             /// compute melting temperature
                    float tm = m_melt.approximate_tm_heterodimer();

                    unsigned long start;
                    unsigned long end;

                    /// Get the length of the alignment
                    std::tie(start, end) = m_melt.alignment_range_target();

                    ///check the match isn't very short and then save it
                    if(end-start>=11){
                        if((max_on_target_results.at(start_pos_p)-tm) <5){
                            mat.at(start_pos_p) = true;
                            mat_rev.at(mat.size()-probe_length-start_pos_p) = true;
                        }

                        if(tm>std::get<3>(thread_results.at(start_pos_p).at(0))){
                            (direction) ? thread_mutex.at(start_pos_p).lock()   : thread_mutex_rev.at(start_pos_p).lock();
                            thread_results.at(start_pos_p).at(0)=std::make_tuple(start_pos_p, start_pos_t + start, start_pos_t + end, tm);
                            (direction) ? thread_mutex.at(start_pos_p).unlock() : thread_mutex_rev.at(start_pos_p).unlock();
                        }
                    }
                }
            }
        }
 
        m_melt.pop_front_target();
	    if(text.at(start_pos_t+probe_length)=='n'){
		      //std::cout <<"break for n" << std::endl;
	        return;
	    }
    }
}

/// Verification of potential matches and calculation of tm
/// \param pattern_start start position of the interval of the pattern to be verified.
/// \param pattern_end end position of the interval of the pattern to be verified.
/// \param text_start start position of the interval of the text to be verified.
/// \param text_end end position of the interval of the text to be verified.
/// \param ignore_start is the start position of a sub interval of (pattern_start, pattern_end) to ignore during verification.
/// \param direction an enum Direction type argument that tells the method which of the genome is being checked.
inline void probe_design_main::verify_area_tm_docking(const unsigned long pattern_start, const unsigned long pattern_end, const unsigned long text_start, const unsigned long text_end, std::vector<bool> &valid_array, const unsigned long ignore_start, const Direction direction){

    NucCruc m_melt = NucCruc(NucCruc::SANTA_LUCIA);
    //m_melt.dangle(false, false);
    m_melt.Salt(0.05);
    m_melt.strand(0.0000009);

    if (pattern_end - pattern_start < probe_length)
        return;
    if (text_end - text_start < probe_length)
        return;

    //std::cout << "target: " << std::endl;


    /// Add possible match from text to the buffer
    for (auto const &cur_char : text.substr(text_start, probe_length)) {
        m_melt.push_back_target(cur_char);
        //std::cout << cur_char;
        if (cur_char == 'n') {
            //std::cout <<"break for n" << std::endl;
            return;
        }
    }

    //std::cout << std::endl << "query: " << std::endl;

    if (!(text_start > (ignore_start + pattern_start - probe_length) &&
          text_start < (ignore_start + pattern_start + probe_length))) {
        for (auto const &cur_char : pattern.substr(pattern_start, 2 * probe_length)) {
            //std::cout << cur_char;
            switch (cur_char){
                case 'a' :
                case 'A' :
                    m_melt.push_front_query('t');
                    break;
                case 't' :
                case 'T' :
                    m_melt.push_front_query('a');
                    break;
                case 'c' :
                case 'C' :
                    m_melt.push_front_query('g');
                    break;
                case 'g' :
                case 'G' :
                    m_melt.push_front_query('c');
                    break;
                case 'n' :
                case 'N' :
                    break;
                default:
                    std::cout << "Unknown base in query sequence" << std::endl;
                    break;
            }
        }
    } else{
        //std::cout << "should be ignored" << std::endl;
        return;
    }

    //std::cout << std::endl;

    /// Compute the Smith-Waterman local alignment between two sequences; the query
    /// and the target. As the alignment is local only the high scoring regions will be
    /// found
    float tm = m_melt.approximate_tm_heterodimer();

    unsigned long start_tar;
    unsigned long end_tar;

    unsigned long start_quer;
    unsigned long end_quer;

    std::tie(start_tar, end_tar)   = m_melt.alignment_range_target();
    std::tie(start_quer, end_quer) = m_melt.alignment_range_query();

    //std::cout << "Length: " << end_tar - start_tar << std::endl;
    //std::cout << "TM: " << tm << std::endl;

    for(auto counter = pattern_start + end_quer-(probe_length); counter< pattern_start + start_quer + probe_length; ++counter){
        if (tm > std::get<3>(thread_results.at(pattern_start+start_quer).at(0))) {
            (direction) ? thread_mutex.at(pattern_start+start_quer).lock() : thread_mutex_rev.at(pattern_start+start_quer).lock();
            thread_results.at(pattern_start+start_quer).at(0) = std::make_tuple(pattern_start+start_quer, text_start + start_tar,
                                                                                text_start + end_tar, tm);
            (direction) ? thread_mutex.at(pattern_start+start_quer).unlock() : thread_mutex_rev.at(pattern_start+start_quer).unlock();
        }

        if ((max_on_target_results.at(pattern_start) - tm) < 3) {
            mat.at(pattern_start) = true;
            mat_rev.at(mat.size() - probe_length - pattern_start) = true;
        }
    }
}

///
/// \param kmer_length is the length of the kmers that were used in the matching phase.
/// \param current_node is a valid node in the suffix tree. Every leaf in the subtree rooted at current_node will be verified.
/// \param frag_id the start position of the kmer that was matched.
/// \param start is the start position of an interval of the pattern that should be ignored during verified.
/// \param direction an enum Direction type argument that tells the method which of the genome is being checked.
inline void probe_design_main::process_suffix_tree_node(const unsigned int kmer_length, const node_type current_node, const unsigned long frag_id, const unsigned int start, const Direction direction){

    auto boundary     = stree.lb(current_node)+1;
    auto end_leaf     = stree.rb(current_node)+1;

    auto cur_pattern         = (direction==Forward) ? getPattern() : getPatRev();
    auto cur_mat             = (direction==Forward) ? getMat() : getMatRev();
    auto cur_thread_results  = (direction==Forward) ? getThreadResults() : getThreadResultsRev();

    unsigned long start_pattern = ((probe_length-kmer_length)>frag_id)  ? 0 : frag_id-(probe_length-kmer_length);
    unsigned long end_pattern   = ((frag_id+probe_length-1)>cur_pattern.length()-1)  ? cur_pattern.length()-1 : frag_id+probe_length-1;

    do{
	    assert(boundary>0);
    	assert(boundary<=stree.size(stree.get_root()));

        auto current_leaf = stree.select_leaf(boundary);
        ++boundary;

        unsigned long suffix_number = stree.sn(current_leaf);
        unsigned long start_text    = ((probe_length-kmer_length)>suffix_number) ? 0 : suffix_number-(probe_length-kmer_length);
        unsigned long end_text      = ((suffix_number+probe_length-1)>text.length()-1) ? text.length()-1 : suffix_number+probe_length-1;

        if(docking_handles.empty())
            verify_area_tm_test(start_pattern,end_pattern,start_text,end_text,cur_mat,start, direction);
            //verify_area_tm(start_pattern,end_pattern,start_text,end_text,cur_mat,start, direction);
        else
            verify_area_tm_docking(start_pattern,end_pattern,start_text,end_text,cur_mat,start, direction);
    } while(boundary <= end_leaf);
}

///
/// \param start_proc is the start of an interval of (0, marked_nodes_forward.size()-1) which is to be processed by the current thread.
/// \param end_proc is the end of an interval of (0,marked_nodes_forward.size()-1) which is to be processed by the current thread.
/// \param kmer_length is the kmer length used in the matching process.
/// \param start is the start of an interval of the pattern to be ignored during varification
inline void probe_design_main::par_process_list(const unsigned int start_proc, const unsigned int end_proc, const unsigned int kmer_length, const unsigned long start){

    for(auto count = start_proc; count<=end_proc; ++count){

        if(marked_nodes_forward.at(count) != stree.get_root()){
            process_suffix_tree_node(kmer_length, marked_nodes_forward.at(count), count, start, Forward);
        }

        if(marked_nodes_backward.at(count) != stree.get_root()){
            process_suffix_tree_node(kmer_length, marked_nodes_backward.at(count), count, start, Reverse);
        }
    }
}

///
/// \param exact_melt a vector containing at each i the exact melting temperature of the kmer length factor starting at i.
/// \param off_target a vector containing at each i the highest off target melting temperature found for the kmer length factor starting at i.
/// \param under_kmer is a variable that is not reliable
/// \param kmer is the length of the seed match
/// \param start
/// \param end
void probe_design_main::output_tables( const unsigned int kmer, const unsigned int start, const unsigned int end) {

    std::cout << "Starting output: " << start << std::endl;

    auto chr_index =0;
    auto region_length = end - start;

    for(auto counter =0; counter<chrs.size(); ++counter){
        std::cout << "prefix: " << prefixsum.at(counter) << std::endl;

        if(prefixsum.at(counter) > start){
            std::cout << "counter final: " << counter << std::endl;
            chr_index = counter;
            break;
        }
    }

    auto new_start = (chr_index==0) ? start : start - prefixsum.at(chr_index-1);

    std::ofstream outputFile("probe_dataset_" + chrs.at(chr_index) + "_" + std::to_string(new_start) + "_" + std::to_string(new_start + region_length) + "_" + std::to_string(kmer) + "_" + std::to_string(probe_length) + "nt" + ".csv");


}

/// 
/// \param exact_melt a vector containing at each i the exact melting temperature of the kmer length factor starting at i.
/// \param off_target a vector containing at each i the highest off target melting temperature found for the kmer length factor starting at i.
/// \param under_kmer is a variable that is not reliable
/// \param kmer is the length of the seed match
/// \param start
/// \param end
void probe_design_main::output_tables(const std::vector<float> &exact_melt, const std::vector<melting_data > &off_target, const std::vector<bool> &under_kmer, const unsigned int kmer, const unsigned int start, const unsigned int end) {

    std::cout << "Starting output: " << start << std::endl;

    auto chr_index =0;
    auto region_length = end - start;

    for(auto counter =0; counter<chrs.size(); ++counter){
        std::cout << "prefix: " << prefixsum.at(counter) << std::endl;

        if(prefixsum.at(counter) > start){
            std::cout << "counter final: " << counter << std::endl;
            chr_index = counter;
            break;
        }
    }

    auto new_start = (chr_index==0) ? start : start - prefixsum.at(chr_index-1);

    std::ofstream outputFile("probe_dataset_" + chrs.at(chr_index) + "_" + std::to_string(new_start) + "_" + std::to_string(new_start + region_length) + "_" + std::to_string(kmer) + "_" + std::to_string(probe_length) + "nt" + ".csv");

    for(int counter = 0; counter < (exact_melt.size())-probe_length; ++counter){
        std::cout << mat.at(counter);
        if(!mat.at(counter)){
            outputFile << chrs.at(chr_index) << "," << counter << "," << pattern.substr(counter, probe_length) << "," << exact_melt.at(counter) << "," << "O" << counter << "," << std::get<2>(off_target.at(counter)) - std::get<1>(off_target.at(counter)) << "," << std::get<3>(off_target.at(counter)) << "," << std::get<1>(off_target.at(counter)) << ","<<  std::get<2>(off_target.at(counter)) << "," << under_kmer.at(counter) << std::endl;
        }
    }
}

/// Primary method for generating probe library.
/// \param start
/// \param end
/// \param short_kmer_length
/// \param long_kmer_length
/// \return
int probe_design_main::probe_design(const unsigned int start, const unsigned int end, const unsigned int short_kmer_length, const unsigned int long_kmer_length) {

    unsigned long const hardware_threads = std::thread::hardware_concurrency();
    unsigned long max_thread = (hardware_threads!=1) ? hardware_threads-1 : 1;

    std::cout << "Hardware Threads " << hardware_threads << std::endl;
    std::cout << "Max Thread "       << max_thread       << std::endl;
    std::cout << "kmer length "      << short_kmer_length      << std::endl;
    std::cout << "start: "           << start            << std::endl;

    std::transform(pattern.begin(), pattern.end(), pattern.begin(), ::tolower);

    std::cout << pattern << std::endl;
    std::cout << pattern.length() << std::endl;

    NucCruc melt = NucCruc();
    melt.Salt(0.05);
    melt.strand(0.0000009);
    melt.dinkelbach(true);


    std::cout << "Compute on target" << std::endl;
    max_on_target_results = std::vector<float>(pattern.length());

    for(auto counter=0; counter<max_on_target_results.size()-probe_length+1; ++counter) {

        std::string probe = pattern.substr(counter,probe_length);
        if(std::count(probe.begin(),probe.end(),'n')==0){

            max_on_target_results.at(counter) = melt.tm_pm_duplex(pattern.substr(counter,probe_length));
        } else {
            max_on_target_results.at(counter) = 300;
        }
    }

    std::cout << "Simple Filters" << std::endl;

    std::vector<bool> under_kmer(pattern.length());

    ///Run simple filters on the region to rule out unlikely areas
    simple_filters filter_helper = simple_filters();

    std::cout << "GC and rep filter" << std::endl;
    filter_helper.gc_and_rep_filter(probe_length, pattern, mat, mat_rev);

    std::cout << "Exclude list filter" << std::endl;
    filter_helper.exclude_list_filter(probe_length, exclude_list, mat, mat_rev);

    std::cout << "kmer count " << std::endl;

    auto kmer_count = stree.kmer_count(pattern,short_kmer_length);
    std::cout << "kmer count filter " << std::endl;

    filter_helper.kmer_count_filter(probe_length,short_kmer_length,kmer_count, mat, mat_rev);
    std::cout << "Size " << stree.size(stree.get_root())<< std::endl;

    std::vector<unsigned long> matching_stats_forward(pattern.size());
    std::vector<unsigned long> matching_stats_backward(pattern.size());

    std::cout << "Mark Suffix Tree Nodes for Verification" << std::endl;
    std::tie(matching_stats_forward,  marked_nodes_forward)  = stree.matching_statistics_mark_depth(pattern, long_kmer_length);

    std::cout << "Marked forward" << std::endl;
    std::tie(matching_stats_backward, marked_nodes_backward) = stree.matching_statistics(pat_rev);

    std::cout << "Marked Nodes " << std::endl;
    std::cout << matching_stats_forward.size() << " " << matching_stats_backward.size() << std::endl;

    assert(matching_stats_forward.size()==matching_stats_backward.size());

    filter_helper.rule_out_big_matches(matching_stats_backward, matching_stats_backward, under_kmer, short_kmer_length, long_kmer_length, probe_length, mat, mat_rev);

    std::tie(matching_stats_forward,  marked_nodes_forward)  = stree.matching_statistics_mark_depth(pattern, short_kmer_length);

    std::cout << "After Simple Filters" << std::endl;

    if(std::count(mat.begin(),mat.end(),true)==mat.size()){
        std::cerr << "No valid probes exist" << std::endl;
        output_tables(short_kmer_length,start, end);
        return 1;
    }

    std::vector<std::thread> verify_thread;

    long count_size = 0;
    long count_size_two = 0;


    for(long counter_now =0; counter_now<marked_nodes_forward.size(); ++counter_now){
        std::cout << mat.at(counter_now) << std::endl;
    	if(marked_nodes_forward.at(counter_now)!=stree.get_root() && !mat.at(counter_now)){
	        count_size += stree.size(marked_nodes_forward.at(counter_now));
        }

        if(marked_nodes_backward.at(counter_now)!=stree.get_root() && !mat_rev.at(counter_now)){
            count_size_two += stree.size(marked_nodes_backward.at(counter_now));
        }
    }

    auto block_size2 = (int) std::floor((count_size+count_size_two)/(float) max_thread);

    std::cout << "using block size: " << block_size2 << " from total " << (count_size+count_size_two) << std::endl;

    ///Start all the threads
    unsigned long start_proc = 0;

    for(unsigned long count = 0; count < max_thread; ++count){
        //This is less shit but still not great load balancing. Currently it evenly distributes the number of nodes to check between the threads.
        //Each node might have a very different number of checks.

        auto counter_now = 0;
        auto counter_nonrt = 0;

        while(counter_nonrt < block_size2 && (start_proc+counter_now)<marked_nodes_forward.size()){
            if(marked_nodes_forward.at(start_proc+counter_now)!=stree.get_root()  && !mat.at(start_proc+counter_now)){
                std::cout << stree.size(marked_nodes_forward.at(start_proc+counter_now)) << std::endl;
                counter_nonrt += stree.size(marked_nodes_forward.at(start_proc+counter_now));
            }

            if(marked_nodes_backward.at(start_proc+counter_now)!=stree.get_root()  && !mat_rev.at(start_proc+counter_now)){
                std::cout << stree.size(marked_nodes_backward.at(start_proc+counter_now)) << std::endl;

                counter_nonrt += stree.size(marked_nodes_backward.at(start_proc+counter_now));
            }

            ++counter_now;
        }

        unsigned long end_proc   = (start_proc+counter_now<marked_nodes_forward.size()) ? start_proc+counter_now : marked_nodes_forward.size()-1;

        std::cout << "size of partition: " << counter_nonrt << std::endl;

        verify_thread.emplace_back(std::thread(&probe_design_main::par_process_list, this, start_proc, end_proc, short_kmer_length, start));

        start_proc = end_proc+1;
    }


    /// Join all the threads
    for (auto& th : verify_thread) {
        th.join();
    	std::cout << "Thread Finished" << std::endl;
    }

    std::cout << "After Verify" << std::endl;

    auto size = 0;
    for(auto const & entry : thread_results){
	    size += entry.size();
    }

    std::cout << "Size " << size  << std::endl;

    std::vector<melting_data> max_off_target_results      (pattern.length());
    std::vector<melting_data> max_off_target_results_rev  (pattern.length());
    std::vector<melting_data> max_off_target_results_final(pattern.length());

    for(auto thrd_idx=0; thrd_idx<thread_results.size(); ++thrd_idx) {

        auto max_val_for = 0.0;
        auto max_ind_for = (int) Undefined;

        auto max_val_rev = 0.0;
        auto max_ind_rev = (int) Undefined;

        for(auto rslt_idx=0; rslt_idx<thread_results.at(thrd_idx).size(); ++rslt_idx) {
            if (std::get<3>(thread_results.at(thrd_idx).at(rslt_idx)) > max_val_for) {
                max_val_for = std::get<3>(thread_results.at(thrd_idx).at(rslt_idx));
                max_ind_for = rslt_idx;
            }
        }

        for(auto rslt_idx=0; rslt_idx<thread_results_rev.at(thrd_idx).size(); ++rslt_idx) {
            if(std::get<3>(thread_results_rev.at(thrd_idx).at(rslt_idx))>max_val_rev){
                max_val_rev = std::get<3>(thread_results_rev.at(thrd_idx).at(rslt_idx));
                max_ind_rev = rslt_idx;
            }
        }

        if(max_ind_for!=Undefined){
            max_off_target_results.at(thrd_idx) = thread_results.at(thrd_idx).at(max_ind_for);
        } else {
            max_off_target_results.at(thrd_idx) = std::make_tuple(0,0,0,0.0);
        }

        if(max_ind_rev!=Undefined){
            max_off_target_results_rev.at(thrd_idx) = thread_results_rev.at(thrd_idx).at(max_ind_for);
        } else {
            max_off_target_results_rev.at(thrd_idx) = std::make_tuple(0,0,0,0.0);
        }
    }

    std::cout << "Combine off target results" << std::endl;

    for(int prb_idx=0; prb_idx<(pattern.length()-probe_length); ++prb_idx){
        if(std::get<3>(max_off_target_results_rev.at(pattern.length()-prb_idx-probe_length)) > std::get<3>(max_off_target_results.at(prb_idx))){
            max_off_target_results_final.at(prb_idx) = std::make_tuple(pattern.length()-prb_idx-probe_length, std::get<1>(max_off_target_results_rev.at(pattern.length()-prb_idx-probe_length)), std::get<2>(max_off_target_results_rev.at(pattern.length()-prb_idx-probe_length)), std::get<3>(max_off_target_results_rev.at(pattern.length()-prb_idx-probe_length)));
        } else {
            max_off_target_results_final.at(prb_idx) = max_off_target_results.at(prb_idx);
        }
    }

    std::cout << "Compute on target results" << std::endl;

    output_tables(max_on_target_results, max_off_target_results_final, under_kmer, short_kmer_length, start, end);

    return 1;
}

///
/// \return
const std::vector<bool> &probe_design_main::getMat() const {
    return mat;
}

///
/// \return
const std::vector<bool> &probe_design_main::getMatRev() const {
    return mat_rev;
}

///
/// \return
const std::string &probe_design_main::getPattern() const {
    return pattern;
}

///
/// \return
const std::string &probe_design_main::getPatRev() const {
    return pat_rev;
}

///
/// \return
const std::vector<std::vector<melting_data>> &probe_design_main::getThreadResults() const {
    return thread_results;
}

///
/// \return
const std::vector<std::vector<melting_data>> &probe_design_main::getThreadResultsRev() const {
    return thread_results_rev;
}



