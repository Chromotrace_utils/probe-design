//
// Created by Odyss on 17/03/2020.
//

#include "read_fasta_files.h"
#include <fstream>
#include <iostream>

read_fasta_files::read_fasta_files() {}

std::tuple<std::vector<unsigned int>, std::vector<std::string>, std::string> read_fasta_files::read_fasta_text(
        const std::string &input_location) {
    std::string text = std::string();

    std::vector<unsigned int> prefixsum;
    std::vector<std::string> chrs;

    std::ifstream text_f(input_location, std::ifstream::in);

    if (!text_f.is_open()) {
        std::cout << " Error: Cannot load text file." << std::endl;
        exit(EXIT_FAILURE);
    } else {

        /// Read fasta file into single sequence ignoring seq names
        std::string line;

        while(std::getline(text_f, line)){
            if(line.find_first_of('>') == std::string::npos){
                text.append(line);
            } else {

                if (!text.empty()){
                    prefixsum.push_back(text.length());
                }

                /// Update output info
                chrs.emplace_back(line.substr(1));

            }
        }
        prefixsum.push_back(text.length());
    }

    text_f.close();

    return std::make_tuple(prefixsum, chrs, text);
}

std::string read_fasta_files::read_fasta_pattern(const std::string &input_location) {

    std::ifstream pattern_f(input_location, std::ifstream::in);
    std::string pattern = std::string();
    std::cout << "Pattern Read START " << std::endl;

    if (!pattern_f.is_open()) {
        std::cerr << " Error: Cannot load pattern file." << std::endl;
        exit(EXIT_FAILURE);
    } else {

        /// Read fasta file into single sequence ignoring seq names
        std::string line;
        while(std::getline(pattern_f, line)){

            if(line.find_first_of('>') == std::string::npos) {
                pattern.append(line);
            }
        }
    }

    return pattern;
}

std::string read_fasta_files::read_exclude_mask(const std::string &input_location) {

    std::ifstream exclude_list_f   (input_location, std::ifstream::in);
    std::string exclude_list = std::string();

    if (!exclude_list_f.is_open()) {
        std::cerr << "Cannot find exclude mask."<< std::endl;

    } else {
        std::getline(exclude_list_f, exclude_list);

        if(exclude_list.find_first_not_of(' ') != std::string::npos)
        {
            std::cout << "There's a non-space." << std::endl;
        }
    }

    return exclude_list;
}

std::vector<std::string> read_fasta_files::read_docking_handles(const std::string &input_location){

    std::ifstream docking_handles_f(input_location, std::ifstream::in);
    std::vector<std::string> docking_handles;


    if(!docking_handles_f.is_open()) {

        std::cerr << "Error: Cannot load docking handles." << std::endl;
        std::cout << "No docking handles will be tested against. List of docking handles is of size: " << docking_handles.size() << std::endl;

    } else {
        std::string line;
        while (std::getline(docking_handles_f, line)) {
            if(line.size()>10 && line.find_first_of('>') == std::string::npos)
                docking_handles.push_back(line);
        }
    }

    return docking_handles;
}


