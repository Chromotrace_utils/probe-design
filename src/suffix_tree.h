/// Copyright (c), EMBL, 2017
/// Created by carl on 30/09/16.
///A suffix tree class based on the SDSL
///library providing the basic functionality for
///chromotrace to use the suffix tree.

#ifndef CHROMOTRACE_NEW_SUFFIXTREE_H
#define CHROMOTRACE_NEW_SUFFIXTREE_H
#include <stdio.h>
#include <sdsl/suffix_trees.hpp>
#include <cstring>
#include <zconf.h>
#include "limits.h"
#include "sdsl/io.hpp"

using namespace sdsl;

//Typedefs for convenience
typedef sdsl::cst_sada<>::node_type node_type;
typedef sdsl::cst_sada<> cst_t;
typedef sdsl::int_vector<>::size_type size_type;

class suffix_tree {

private:

    enum Ambiguous_Matching_Location: unsigned long {//Enum to define an ambiguous location in matching statistics
        Ambiguous_Loc = ULONG_MAX
    };

    cst_t cst;
    unsigned long text_length;

public:
    suffix_tree() : text_length(0), cst() {}

    explicit suffix_tree(const std::string& text) {
        std::cout << "Starting suffix tree constructor from: " << text << std::endl;

        bool worked = load_from_file(cst, text);

        if(!worked)
        {
            std::cout << "Error loading suffix tree" << std::endl;
            exit(1);
        } else {
	        std::cout << "Suffix tree loaded of size: " << cst.size() << std::endl;
	    }

        text_length = cst.size();
        std::cout << "Finished suffix tree constructor" << std::endl;
    }


    unsigned long pattern_match(const std::string& pattern) const;//Match pattern from the root
    unsigned long pattern_match(const std::string& pattern, node_type node) const;//Match pattern from the specified node
    unsigned long get_text_length() const;

    std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> matching_statistics(std::string pattern) const;//compute matching statistics for pattern
    std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> matching_statistics_mark_depth(std::string pattern, unsigned long first_depth) const;//compute matching statistics for pattern
    std::vector<unsigned long> kmer_count( std::string pattern, unsigned long first_depth) const;

    node_type leftmost_leaf(node_type node)  const;
    node_type rightmost_leaf(node_type node) const;
    node_type select_leaf(size_type node)    const;
    node_type sibling(node_type node) const;

    size_type lb(node_type node) const;
    size_type rb(node_type node) const;
    size_type nodes() const;
    size_type sn(node_type node) const;
    size_type size(node_type node) const;

    node_type is_unique(const std::string& pattern) const;
    node_type is_unique(const std::string& pattern, unsigned long depth) const;//Simulate truncated suffix trees by only doing this for fixed depth
    node_type get_suffix_leaf(unsigned long suffix) const;
    node_type get_parent(node_type node) const;
    node_type get_root() const;

    std::string get_suffix(unsigned long suffix, unsigned long length);
    std::string get_suffix(unsigned long suffix);

    size_type get_size() const;
    bool is_root(node_type node) const;
    bool valid_pos(unsigned long position) const;


};


#endif //CHROMOTRACE_ORIGINAL_SUFFIX_TREE_H
