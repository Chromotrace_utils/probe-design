//
// Created by Odyss on 17/03/2020.
//

#include <iostream>
#include "simple_filters.h"



void simple_filters::exclude_list_filter(unsigned int probe_length, const std::string &exclude_list, std::vector<bool> &mat,  std::vector<bool> &mat_rev) {

    ///
    /// Exclude positions based on the exclude list
    ///
    for(auto count =0; count<exclude_list.length(); ++count){
        if(exclude_list.at(count)=='1'){
            mat.at(count) = true;
        }
    }

    std::cout << "SYNC " << exclude_list.length() << std::endl;
    ///
    /// Sync mat and mat_rev
    ///

    for(auto count =0; count<exclude_list.length()-probe_length+1; ++count){

        if(mat.at(count)==true){
            mat_rev.at(exclude_list.length()-count-probe_length)  = true;
        }
    }

    ///
    /// Exclude positions that represent sequences of length < probe_length
    ///
    for(auto count =(exclude_list.length()-probe_length); count<exclude_list.length(); ++count){
        mat.at(count)     = true;
        mat_rev.at(count) = true;
    }
}

///
/// Run simple filters based on GC, masked reps and Ns in the sequence.
///

void simple_filters::gc_and_rep_filter(unsigned int probe_length, std::string &pattern, std::vector<bool> &mat,  std::vector<bool> &mat_rev) {

    unsigned int gc      = 0;
    unsigned int rep     = 0;
    unsigned int n_count = 0;

    for( auto const & cur_char : pattern.substr(0,probe_length)){
        switch (cur_char) {
            case 'C':
                ++rep;
            case 'c':
                ++gc;
                break;
            case 'G':
                ++rep;
            case 'g':
                ++gc;
                break;
            case 'A':
            case 'T':
                ++rep;
                break;
            case 'n':
            case 'N':
                ++n_count;
                break;
            default:
                break;
        }
    }

    if(gc<(0.3*((float)probe_length)) || gc>(0.7*((float)probe_length))){
        mat.at(0)  = true;
        mat_rev.at(pattern.length()-probe_length)  = true;
    }

    if(n_count>0){

        mat.at(0) = true;
        mat_rev.at(pattern.length()-probe_length)  = true;
    }

    int limit = pattern.length()-probe_length;

    for(auto count = 1; count < limit; ++count){

        switch (pattern.at(count-1)) {
            case 'C':
                --rep;
            case 'c':
                --gc;
                break;
            case 'G':
                --rep;
            case 'g':
                --gc;
                break;
            case 'A':
            case 'T':
                --rep;
                break;
            case 'n':
            case 'N':
                --n_count;
                break;
            default:
                break;
        }

        switch (pattern.at(count+probe_length-1)) {
            case 'C':
                ++rep;
            case 'c':
                ++gc;
                break;
            case 'G':
                ++rep;
            case 'g':
                ++gc;
                break;
            case 'A':
            case 'T':
                ++rep;
                break;
            case 'n':
            case 'N':
                ++n_count;
                break;
            default:
                break;
        }

        if(gc<(0.3*((float)probe_length)) || gc>(0.7*((float)probe_length))){
            for(int invalid = count-(probe_length); invalid<count; ++invalid){
                mat.at(count)  = true;
                mat_rev.at(pattern.length()-count-probe_length)  = true;
            }
        }

        if(n_count>0){
            mat.at(count) = true;
            mat_rev.at(pattern.length()-count-probe_length)  = true;
        }
    }

    ///
    /// Exclude positions that represent sequences of length < probe_length
    ///
    for(auto count =(pattern.length()-probe_length+1); count<pattern.length(); ++count){
        mat.at(count)     = true;
        mat_rev.at(count) = true;
    }
}

/// Run a simple filter to rule out very long matches between the text and query
/// \param matching_stats_forward
/// \param matching_stats_backward
/// \param under_kmer
/// \param kmer_length
/// \param kmer_length_2
void simple_filters::rule_out_big_matches(std::vector<unsigned long> &matching_stats_forward,
                                          std::vector<unsigned long> &matching_stats_backward,
                                          std::vector<bool> &under_kmer, unsigned int kmer_length,
                                          unsigned int kmer_length_2, unsigned int probe_length, std::vector<bool> &mat, std::vector<bool> &mat_rev) {

    auto over_fwd = 0;
    auto over_bwd = 0;
    auto under_kmer_fwd = 0;
    auto under_kmer_bwd = 0;

    if(kmer_length>probe_length){
        std::cerr << "Invalid kmer length and probe_length. kmer_length is bigger than the probe_length" << std::endl;
        return;
    }

    for( auto count =0; count<probe_length-kmer_length+1; ++count){

        if (matching_stats_forward.at(count) >= kmer_length_2) {
            ++over_fwd;
        }

        if (matching_stats_backward.at(count) >= kmer_length_2) {
            ++over_bwd;
        }

        if (matching_stats_forward.at(count) >= kmer_length) {
            ++under_kmer_fwd;
        }

        if (matching_stats_backward.at(count) >= kmer_length) {
            ++under_kmer_bwd;
        }
    }

    if(over_fwd>0 || over_bwd>0){
        std::cout << "REULE OUT NUMBER ONE" << std::endl;
        mat.at(0)  = true;
        mat_rev.at(mat.size()-probe_length)  = true;
    }

    if(under_kmer_fwd>0){
        under_kmer.at(0) = true;
    }

    if(under_kmer_bwd>0){
        under_kmer.at(matching_stats_forward.size()-probe_length)  = true;
    }

    int limit = matching_stats_forward.size()-probe_length+1;

    for(auto count = 1; count < limit; ++count){

        if(matching_stats_forward.at(count-1) >= kmer_length_2) {
            --over_fwd;
        }

        if(matching_stats_backward.at(count-1) >= kmer_length_2) {
            --over_bwd;
        }

        if (matching_stats_forward.at(count-1) >= kmer_length) {//This works
            --under_kmer_fwd;
        }

        if (matching_stats_backward.at(count-1) >= kmer_length) {//This works
            --under_kmer_bwd;
        }


        if (matching_stats_forward.at(count+probe_length-kmer_length) >= kmer_length_2) {
            ++over_fwd;
        }

        if (matching_stats_backward.at(count+probe_length-kmer_length) >= kmer_length_2) {
            ++over_bwd;
        }

        if (matching_stats_forward.at(count+probe_length-kmer_length) >= kmer_length) {
            ++under_kmer_fwd;
        }

        if (matching_stats_backward.at(count+probe_length-kmer_length) >= kmer_length) {
            ++under_kmer_bwd;
        }


        if(over_fwd>0 || over_bwd>0){
            mat.at(count)  = true;
            mat_rev.at(matching_stats_forward.size()-count-probe_length)  = true;
        }

        if(under_kmer_fwd>0){
            under_kmer.at(count) = true;
        }

        if(under_kmer_bwd>0){
            under_kmer.at(matching_stats_forward.size()-count-probe_length)  = true;
        }

    }

}

void simple_filters::kmer_count_filter(unsigned int probe_length, unsigned int kmer_length,
                                       std::vector<unsigned long> &kmer_count, std::vector<bool> &mat,
                                       std::vector<bool> &mat_rev) {

    auto over = 0;

    if(kmer_length>probe_length){
        std::cerr << "Invalid kmer length and probe_length. kmer_length is bigger than the probe_length" << std::endl;
        return;
    }

    for( auto count =0; count<probe_length-kmer_length+1; ++count){

        if (kmer_count.at(count)>255) {
            ++over;
        }
    }

    if(over>0.5*probe_length){
        mat.at(0)  = true;
        mat_rev.at(mat.size()-probe_length)  = true;
    }

    int limit = kmer_count.size()-probe_length+1;

    for(auto count = 1; count < limit; ++count){

        if(kmer_count.at(count-1)>255) {
            --over;
        }

        if (kmer_count.at(count+probe_length-kmer_length)>255) {
            ++over;
        }


        if(over>0.5*probe_length){
            mat.at(count)  = true;
            mat_rev.at(kmer_count.size()-count-probe_length)  = true;
        }

    }

    ///
    /// Exclude positions that represent sequences of length < probe_length
    ///
    for(auto count =(kmer_count.size()-probe_length+1); count<kmer_count.size(); ++count){
        mat.at(count)     = true;
        mat_rev.at(count) = true;
    }
}
