//
// Created by Odyss on 17/03/2020.
//

#ifndef PROBE_DESIGN_READ_FASTA_FILES_H
#define PROBE_DESIGN_READ_FASTA_FILES_H


#include <string>
#include <vector>
#include <tuple>

class read_fasta_files {
public:
    read_fasta_files();

    std::tuple<std::vector<unsigned int>, std::vector<std::string>, std::string> read_fasta_text(const std::string &input_location);
    std::vector<std::string> read_docking_handles(const std::string &input_location);
    std::string read_fasta_pattern(const std::string &input_location);
    std::string read_exclude_mask(const std::string &input_location);

};


#endif //PROBE_DESIGN_READ_FASTA_FILES_H
