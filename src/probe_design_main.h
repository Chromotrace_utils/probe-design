//
// Created by carl on 26/09/17.
//

#ifndef PROBE_DESIGN_MAIN_H
#define PROBE_DESIGN_MAIN_H

#include <mutex>
#include <iostream>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <cstring>
#include <math.h>
#include <algorithm>
#include "suffix_tree.h"
#include "read_fasta_files.h"
#include "nuc_cruc.h"
#include <thread>

// typedef for storing melting temp results as: probe start pos, off target start pos, off target end pos, melting temp
typedef std::tuple<unsigned long, unsigned long, unsigned long, float> melting_data;

class probe_design_main{

private:

    unsigned int probe_length;

    std::vector<std::mutex>  thread_mutex;
    std::vector<std::mutex>  thread_mutex_rev;

    std::vector<std::vector<melting_data>> thread_results;
    std::vector<std::vector<melting_data>> thread_results_rev;

    //Store results of thread computation. the tuple i probe start pos, off target start, off target end, melting temp

    std::vector<bool> mat;
    std::vector<bool> mat_rev;

    std::string text;
    std::string pattern;
    std::string pat_rev;

    std::vector<unsigned long> marked_nodes_forward;
    std::vector<unsigned long> marked_nodes_backward;

    std::vector<float> max_on_target_results;

    std::vector<std::string> docking_handles;
    std::string exclude_list;

    std::vector<unsigned int> prefixsum;
    std::vector<std::string> chrs;

    suffix_tree stree;

    enum Direction: bool {//Enum to define pseudo directions
        Forward  = true,
        Reverse  = false
    };

    enum Index: int{//Enum to define invalid array index
        Undefined  = -1
    };

    int probe_design(unsigned int start, unsigned int end, unsigned int short_kmer_length, unsigned int long_kmer_length);
    void output_tables(const std::vector<float> &exact_melt, const std::vector<melting_data> &off_target, const std::vector<bool> &under_kmer, unsigned int kmer, unsigned int start, unsigned int end);
    void output_tables( unsigned int kmer, unsigned int start, unsigned int end);

    void par_process_list(unsigned int start_proc, unsigned int end_proc, unsigned int kmer_length, unsigned long start);
    void process_suffix_tree_node(unsigned int kmer_length, node_type current_node, unsigned long frag_id, unsigned int start, Direction direction);

    void verify_area_tm(unsigned long pattern_start, unsigned long pattern_end, unsigned long text_start, unsigned long text_end, std::vector<bool> &valid_array, unsigned long ignore_start, Direction direction);
    void verify_area_tm_test(unsigned long pattern_start, unsigned long pattern_end, unsigned long text_start, unsigned long text_end, std::vector<bool> &valid_array, unsigned long ignore_start, Direction direction);
    void verify_area_tm_docking(unsigned long pattern_start, unsigned long pattern_end, unsigned long text_start, unsigned long text_end, std::vector<bool> &valid_array, unsigned long ignore_start, Direction direction);

    const std::vector<bool> &getMat() const;
    const std::vector<bool> &getMatRev() const;
    const std::string &getPattern() const;
    const std::string &getPatRev() const;
    const std::vector<std::vector<melting_data>> &getThreadResults() const;
    const std::vector<std::vector<melting_data>> &getThreadResultsRev() const;


public:
    probe_design_main(const std::string& input_pattern_fasta, const std::string& input_text_fasta, const std::string& input_text_index, const unsigned int probe_len=40,
                      const unsigned int kmer_length=16, const unsigned int kmer_length_2=24, const std::string& exclusion_list=std::string(), const std::string& docking_handle=std::string()) : probe_length(probe_len) {

    	std::cout << "Try to open input streams "<< std::endl;

    	std::cout << "Opening Files" << std::endl;

    	read_fasta_files io_helper = read_fasta_files();

    	std::tie(prefixsum, chrs, text) = io_helper.read_fasta_text(input_text_fasta);

        assert(prefixsum.size()==chrs.size());
	    std::cout << "Text is of length: " << text.length() << std::endl;

	    pattern = io_helper.read_fasta_pattern(input_pattern_fasta);
        std::string pattern_low(pattern);

        std::transform(pattern_low.begin(), pattern_low.end(), pattern_low.begin(), ::tolower);
        std::transform(text.begin(),    text.end(),    text.begin(),    ::tolower);

        std::cout << pattern << std::endl;

        unsigned int start_pos = text.find(pattern_low);

        std::cout << "Search for query sequence completed" << std::endl;

        std::cout << "Loaded text" << std::endl;

        if (text.empty()){
            std::cout << " Error: Cannot load text." << std::endl;
            exit(EXIT_FAILURE);
        }

        if (pattern.empty()) {
            std::cerr << " Error: Cannot load pattern." << std::endl;
            exit(EXIT_FAILURE);
        }

        if ( pattern.length() < probe_length )
        {
            std::cerr << " Error: Invalid length of pattern or probe." << std::endl;
            exit(EXIT_FAILURE);
        }

        if(probe_len==0 || probe_len>pattern.length()){
            std::cerr << " Error: invalid probe length must be bigger than 0 and less than pattern.length()" << std::endl;
            exit(EXIT_FAILURE);
        }

        if(probe_length<kmer_length){
            std::cerr << " Error: invalid probe length or kmer_length. kmer_length must be less than probe_length" << std::endl;
            exit(EXIT_FAILURE);
        }

        if(start_pos>text.length()) {
            std::cerr << " Error: Cannot find pattern."  << std::endl;
            exit(EXIT_FAILURE);
        } else {
            std::cout << "Query sequence found at position: " << start_pos << std::endl;
        }

        std::cout << "Loaded pattern" << std::endl;

        exclude_list = io_helper.read_exclude_mask(exclusion_list);

        if(exclude_list.length()!=pattern.length()){
            exclude_list = std::string(pattern.length(),'0');
        }

        std::cout << "exclude_list4 " << pattern.length() << std::endl;

        docking_handles = io_helper.read_docking_handles(docking_handle);

        std::cout << "Initialising mutex and results vectors" << std::endl;

        mat     = std::vector<bool>(pattern.length());
        mat_rev = std::vector<bool>(pattern.length());

        thread_mutex       = std::vector<std::mutex>(pattern.length());
        thread_mutex_rev   = std::vector<std::mutex>(pattern.length());

        thread_results     = std::vector<std::vector<melting_data>>(pattern.length());
        thread_results_rev = std::vector<std::vector<melting_data>>(pattern.length());

        for(auto temp = 0; temp<pattern.length();++temp){
            thread_results.at(temp).push_back(std::make_tuple(0,0,0,0.0));
            thread_results_rev.at(temp).push_back(std::make_tuple(0,0,0,0.0));
        }

        std::cout << "Finished initialising mutex and results vectors" << std::endl;

        std::cout << "Load Suffix Tree" << std::endl;

        stree = suffix_tree(input_text_index);

        std::cout << "Suffix Tree Loaded" << std::endl;

        std::cout << "Start search for query sequence in genome sequence" << std::endl;

        pat_rev = std::string(pattern);
        std::reverse(pat_rev.begin(), pat_rev.end());

        for(int count =0; count <pat_rev.size();++count){
            if(pat_rev.at(count)=='c'){
                pat_rev.at(count) = 'g';
            } else if(pat_rev.at(count)=='g'){
                pat_rev.at(count) = 'c';
            } else if(pat_rev.at(count)=='a'){
                pat_rev.at(count) = 't';
            } else if(pat_rev.at(count)=='t'){
                pat_rev.at(count) = 'a';
            }
        }

        std::cout << "this is before we call the main method: " << exclude_list.size()<< std::endl;
        std::cout << "pattern length "  << pattern.length()     << std::endl;
    	std::cout << start_pos << " - " << (start_pos+pattern.size()-1)  << std::endl;

        probe_design(start_pos, start_pos+pattern.size()-1, kmer_length, kmer_length_2);
    }
};

#endif //PROBE_DESIGN_MAIN_H
