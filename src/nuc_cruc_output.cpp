// ThermonucleotideBLAST
// 
// Copyright (c) 2008, Los Alamos National Security, LLC
// All rights reserved.
// 
// Copyright 2007. Los Alamos National Security, LLC. This software was produced under U.S. Government 
// contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos 
// National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, 
// reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, 
// LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  
// If software is modified to produce derivative works, such modified software should be clearly marked, 
// so as not to confuse it with the version available from LANL.
// 
// Additionally, redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
// 
//      * Redistributions of source code must retain the above copyright notice, this list of conditions 
//        and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the distribution.
//      * Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, 
//        the U.S. Government, nor the names of its contributors may be used to endorse or promote products 
//        derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY 
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC 
// OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "nuc_cruc.h"
#include <iostream>
#include <sstream>

using namespace std;

ostream& operator << (ostream &s, const NucCruc &m_melt)
{
	// Order given by enum {A = 0, C, G, T, I, E}
	const char* base_map = "ACGTI$-";
	
	if( m_melt.curr_align.query_align.size() != m_melt.curr_align.target_align.size() ){
		throw __FILE__ ":operator <<: alignment size mismatch!";
	}
	
	if(m_melt.tm_mode == NucCruc::HAIRPIN){
		
		s << "5' ";
	
		deque<BASE::nucleic_acid>::const_reverse_iterator q_iter, t_iter;

		for(t_iter = m_melt.curr_align.target_align.rbegin();t_iter != m_melt.curr_align.target_align.rend();t_iter++){

			s << base_map[*t_iter];
		}

		s << endl;

		t_iter = m_melt.curr_align.target_align.rbegin();

		s << "   ";

		for(q_iter = m_melt.curr_align.query_align.rbegin();q_iter != m_melt.curr_align.query_align.rend();q_iter++,t_iter++){

			enum {NO_MATCH, MATCH, INOSINE_MATCH};

			unsigned int match = NO_MATCH;

			switch(*q_iter){
				case BASE::A:
					match = (*t_iter == BASE::T) ? MATCH : NO_MATCH;
					break;
				case BASE::T:
					match = (*t_iter == BASE::A) ? MATCH : NO_MATCH;
					break;
				case BASE::G:
					match = (*t_iter == BASE::C) ? MATCH : NO_MATCH;
					break;
				case BASE::C:
					match = (*t_iter == BASE::G) ? MATCH : NO_MATCH;
					break;
				case BASE::I:
					match = (*t_iter == BASE::GAP) ? NO_MATCH : INOSINE_MATCH;
					break;
				case BASE::E:
				case BASE::GAP:
					match = NO_MATCH;
					break;
			};

			if( (*t_iter == BASE::I) && (*q_iter != BASE::GAP) ){
				match = INOSINE_MATCH;
			}

			switch(match){
				case NO_MATCH:
					s << ' ';
					break;
				case MATCH:
					s << '|';
					break;
				case INOSINE_MATCH:
					s << '*';
					break;
			};
		}

		s << "\n3' " ;

		for(q_iter = m_melt.curr_align.query_align.rbegin();q_iter != m_melt.curr_align.query_align.rend();q_iter++){
			s << base_map[*q_iter];
		}

		s << "\nhairpin";
	}
	else{ // m_melt.tm_mode != NucCruc::HAIRPIN
	
		s << "5' ";
	
		deque<BASE::nucleic_acid>::const_iterator q_iter, t_iter;

		for(q_iter = m_melt.curr_align.query_align.begin();q_iter != m_melt.curr_align.query_align.end();q_iter++){

			s << base_map[*q_iter];
		}

		s << " 3'" << endl;

		q_iter = m_melt.curr_align.query_align.begin();

		s << "   ";

		for(t_iter = m_melt.curr_align.target_align.begin();t_iter != m_melt.curr_align.target_align.end();t_iter++,q_iter++){

			enum {NO_MATCH, MATCH, INOSINE_MATCH};

			unsigned int match = NO_MATCH;

			switch(*t_iter){
				case BASE::A:
					match = (*q_iter == BASE::T) ? MATCH : NO_MATCH;
					break;
				case BASE::T:
					match = (*q_iter == BASE::A) ? MATCH : NO_MATCH;
					break;
				case BASE::G:
					match = (*q_iter == BASE::C) ? MATCH : NO_MATCH;
					break;
				case BASE::C:
					match = (*q_iter == BASE::G) ? MATCH : NO_MATCH;
					break;
				case BASE::I:
					match = (*q_iter == BASE::GAP) ? NO_MATCH : INOSINE_MATCH;
					break;
				case BASE::E:
				case BASE::GAP:
					match = NO_MATCH;
					break;
			};

			if( (*q_iter == BASE::I) && (*t_iter != BASE::GAP) ){
				match = INOSINE_MATCH;
			}

			switch(match){
				case NO_MATCH:
					s << ' ';
					break;
				case MATCH:
					s << '|';
					break;
				case INOSINE_MATCH:
					s << '*';
					break;
			};
		}

		s << endl;

		s << "3' " ;

		for(t_iter = m_melt.curr_align.target_align.begin();t_iter != m_melt.curr_align.target_align.end();t_iter++){
			s << base_map[*t_iter];
		}

		s << " 5'" << endl;

		s << "dimer";
	}
	
	s << " alignment size = " << m_melt.curr_align.query_align.size();
	
	return s;
}

string NucCruc::query_seq() const
{
	// Order given by enum {A = 0, C, G, T, I, E}
	const char* base_map = "ACGTI$";
	CircleBuffer<BASE::nucleic_acid, MAX_SEQUENCE_LENGTH>::const_iterator iter;
	
	stringstream sout;
	
	for(iter = query.begin();iter != query.end();iter++){
		sout << base_map[*iter];
	}
	
	return sout.str();
}

string NucCruc::target_seq() const
{

	// Order given by enum {A = 0, C, G, T, I, E}
	const char* base_map = "ACGTI$";
	CircleBuffer<BASE::nucleic_acid, MAX_SEQUENCE_LENGTH>::const_iterator iter;
	
	stringstream sout;
	
	for(iter = target.begin();iter != target.end();iter++){
		sout << base_map[*iter];
	}
	
	return sout.str();
}
