#include <sdsl/suffix_trees.hpp>
#include <iostream>
#include <string>
#include "sdsl/io.hpp"

using namespace std;
using namespace sdsl;

typedef cst_sada<> cst_t;

int main(int argc, char* argv[])
{
    if (argc < 3) {
        cout << "usage: "<<argv[0]<< " input output " << std::endl;
        return 1;
    }

    std::ifstream text_f(argv[1]);

    std::string text;
    std::string line;

    while(std::getline(text_f, line)){
        std::cout << line.find_first_of('>') << std::endl;

        if(line.find_first_of('>') == std::string::npos) {
            text.append(line);
        }
    }

    text.append("#");

    cst_t cst;

    std::cout << "Building Suffix Tree" << std::endl;

    std::string file = "@testinput.iv8";
    store_to_file(text.c_str(), file);
    construct(cst, file,1);

    std::cout << "Suffix tree build success" << std::endl;

    std::cout << "Writing file to disk at location: " << argv[2] << std::endl;
    store_to_file(cst,argv[2]);
    
}
