# Build Suffix Tree

## Install amd Useage

If you have built the project from the root directory then build_suffix_tree will also be built. If you wish to build the project seperately then the following commands can be executed to create an executable file in your build directory.

```
mkdir build
cd build
cmake ..
make
```
The code can be run by providing a (Multi-) FASTA file as input and an output path as follows.

```
./build_suffix_tree hg38.seq hg38.cst
```
