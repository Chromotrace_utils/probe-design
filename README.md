# Genome Probe Computation Using Melting Temperature

Code to compute the off target melting temp for a query region.

CMake must already be installed. This can be found at [CMake](https://cmake.org/).

Make must already be installed. Information can be found at [Make](https://www.tutorialspoint.com/unix_commands/make.htm).

This code uses the SDSL-lite library which is avaliable here [SDSL-lite](https://github.com/simongog/sdsl-lite).
It will automatically be cloned from the github repository and installed by the cmake script. 

## Install amd Useage

To make the project the following commands can be followed to create a binary in your build directory.

```
git clone git@gitlab.com:Chromotrace_utils/probe-design.git
cd probe-design
mkdir build
cd build
cmake ..
make
```

The program can be run by providing between 3-8 arguments as follows..


```
# ./probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst

./probe_design <path to query sequence file> <path to genome sequence> <path to genome index>

# ./probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst 50

./probe_design <path to query sequence file> <path to genome sequence> <path to genome index> <probe sequence length in nt>

# ./probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst 50 17

./probe_design <path to query sequence file> <path to genome sequence> <path to genome index> <probe sequence length in nt> <small kmer length in nt>

# ./probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst 50 17 26

./probe_design <path to query sequence file> <path to genome sequence> <path to genome index> <probe sequence length in nt> <small kmer length in nt> <large kmer length in nt>

# ./probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst 50 17 26 /path/to/exclusion.vec 

./probe_design <path to query sequence file> <path to genome sequence> <path to genome index> <probe sequence length in nt> <small kmer length in nt> <large kmer length in nt> <path to exclusion list>

# ./probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst 50 17 26 /path/to/exclusion.vec /path/to/docking_handle.list

./probe_design <path to query sequence file> <path to genome sequence> <path to genome index> <probe sequence length in nt> <small kmer length in nt> <large kmer length in nt> <path to exclusion file> <path to docking handle file>
       
```

In general if you want to specify If you wise to specify <path to exclusion file> or <path to docking handle file> you must specify the all the preceeding parameters. If you wish to specify, for example, docking_handle.list but not an exclusion list this can be done as follows.

```
./probe_design /path/to/query /path/to/genome.seq /path/to/genome.cst 50 17 26 null /path/to/docking_handle.list
```

## Query and Genome File Example

The genome file and query file should be in FASTA format, see https://en.wikipedia.org/wiki/FASTA_format for more information. 
The query should be a sub string of the genome input and the input FASTA file should contain one sequences. The genome file may be a multi-FASTA file.

## Exclusion List Example

An exclusion list is a single lined, comma seperated file with the same length as the query pattern. Each number should be either 0 (don't exclude probe starting at this position) or 1 (exclude probe starting at this position)

```
111111111111111111111111111111111111111111111111111111111111111111111111111111111
```

## Docker Handle List

The docking handle list should be a file of sequences in lowercase seperated by new lines

```
ttatacatctacgg
tttcttcattagcg
tttcaatgtatggc
ttataatggatggg
tttagttagagccc
ttttgatgatagcc
ttataaagtgtcca
ttatatgatctccg
tttattaagctcgc
ttttaaaacagcct
ttaatacgactcactataggga
```
## Genome Index Format

The genome index used in this program is constructed using the SDSL-lite library for succint data structures.
Any compressed suffix tree from the SDSL-lite library will work but currently we use the type cst_sada provided by the library.

There is a script provided to build the suffix tree in the build_suffix_tree directory.
