//
// Created by Odyss on 18/03/2020.
//

#include <gtest/gtest.h>
#include "../src/read_fasta_files.h"
#include <gmock/gmock.h>

class ReadFastaTest: public ::testing::Test {
public:
    ReadFastaTest( ) {
        // initialization;
        // can also be done in SetUp()
    }

    read_fasta_files io_helpers  = read_fasta_files();
    read_fasta_files io_helpers2 = read_fasta_files();

    void SetUp( ) {

        // initialization or some code to run before each test
    }

    void TearDown( ) {
        // code to run after each test;
        // can be used instead of a destructor,
        // but exceptions can be handled in this function only
    }

    ~ReadFastaTest( )  {
        //resources cleanup, no exceptions allowed
    }

    // shared user data
};

TEST_F(ReadFastaTest, ReadPatternFasta) {
    ASSERT_EQ(io_helpers.read_fasta_pattern("/mnt/c/Users/Odyss/Documents/probe-design/test_data/pattern"), "ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc");
    ASSERT_EQ(io_helpers2.read_fasta_pattern("/mnt/c/Users/Odyss/Documents/probe-design/test_data/pattern2"),"cgtcgttgcatccgtatctctgattctatttcatattcaggccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc");
}

TEST_F(ReadFastaTest, ReadTextFasta) {

    std::vector<unsigned int> prefixsum;
    std::vector<std::string> chrs;
    std::string text;

    std::tie(prefixsum, chrs, text) = io_helpers.read_fasta_text("/mnt/c/Users/Odyss/Documents/probe-design/test_data/text");

    ASSERT_EQ(prefixsum.at(0), 339);
    ASSERT_EQ(chrs.at(0), "chr1");
    ASSERT_EQ(text, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

    std::tie(prefixsum, chrs, text) = io_helpers2.read_fasta_text("/mnt/c/Users/Odyss/Documents/probe-design/test_data/text2");

    ASSERT_EQ(prefixsum.at(0), 293);
    ASSERT_EQ(chrs.at(0), "chr1");
    ASSERT_EQ(text, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaacgtcgttgcatccgtatctctgattctatttcatattcaggcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

}

TEST_F(ReadFastaTest, ReadExclude) {
    ASSERT_EQ(io_helpers.read_exclude_mask("/mnt/c/Users/Odyss/Documents/probe-design/test_data/exclude_list_pass1"), "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
    ASSERT_EQ(io_helpers.read_exclude_mask("/mnt/c/Users/Odyss/Documents/probe-design/test_data/exclude_list_pass2"), "000000000000000001111111100000000000000000000000000000000000000000000000000000000000000000000000000");
    ASSERT_EQ(io_helpers.read_exclude_mask("/mnt/c/Users/Odyss/Documents/probe-design/test_data/exclude_list_pass3"), "000000011111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
}

TEST_F(ReadFastaTest, ReadDockingHandle) {

    std::vector<std::string> docking_handles;
    std::vector<std::string> expected_handles = {"ttatacatctacgg",
            "tttcttcattagcg",
            "tttcaatgtatggc",
            "ttataatggatggg",
            "tttagttagagccc",
            "ttttgatgatagcc",
            "ttataaagtgtcca",
            "ttatatgatctccg",
            "tttattaagctcgc",
            "ttttaaaacagcct",
            "ttaatacgactcactataggga"};

    docking_handles = io_helpers.read_docking_handles("/mnt/c/Users/Odyss/Documents/probe-design/test_data/docking_handles");

    for(auto count = 0; count<docking_handles.size(); ++count){
        ASSERT_EQ(docking_handles.at(count), expected_handles.at(count));
    }

}