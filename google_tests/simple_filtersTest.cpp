//
// Created by Odyss on 18/03/2020.
//

#include <gtest/gtest.h>
#include "../src/read_fasta_files.h"
#include "../src/simple_filters.h"
#include <gmock/gmock.h>

class SimpleFilterTest: public ::testing::Test {
public:
    SimpleFilterTest( ) {
        // initialization;
        // can also be done in SetUp()
    }

    simple_filters filter_helpers  = simple_filters();

    void SetUp( ) {

        // initialization or some code to run before each test
    }

    void TearDown( ) {
        // code to run after each test;
        // can be used instead of a destructor,
        // but exceptions can be handled in this function only
    }

    ~SimpleFilterTest( )  {
        //resources cleanup, no exceptions allowed
    }

    // shared user data
};

TEST_F(SimpleFilterTest, GCRepTest){

    std::string pattern("aaaaaaagcgcgcgcccgggccgggccggtttttttt");

    std::vector<bool> mat    (pattern.length(), false);
    std::vector<bool> mat_rev(pattern.length(), false);

    std::vector<bool> expected_mat     = {false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true};
    std::vector<bool> expected_mat_rev = {false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false,false,false,true,true,true,true,true,true,true,true,true};

    filter_helpers.gc_and_rep_filter(10, pattern, mat, mat_rev);

    for(auto count=0; count<pattern.length(); ++count){
        ASSERT_EQ(mat.at(count), expected_mat.at(count));
        ASSERT_EQ(mat_rev.at(count), expected_mat_rev.at(count));
    }

    pattern = std::string("tgctggtNNNNNNNttgc");

    mat     = std::vector<bool>(pattern.length(), false);
    mat_rev = std::vector<bool>(pattern.length(), false);

    expected_mat     = {false,true,true,false,true,true,true,true,true,true,true,true,true,true,false,true,true,true};
    expected_mat_rev = {false,true,true,true,true,true,true,true,true,true,true,false,true,true,false,true,true,true};

    filter_helpers.gc_and_rep_filter(4, pattern, mat, mat_rev);

    for(auto count=0; count<pattern.length(); ++count){
        ASSERT_EQ(mat.at(count), expected_mat.at(count));
        ASSERT_EQ(mat_rev.at(count), expected_mat_rev.at(count));
    }
}

TEST_F(SimpleFilterTest, RuleOutBigMatchTest){

    std::vector<unsigned long> matching_stats_forward  = {1,3,5,7,9,11,13,15,17,19,6,5,4,3,2,1};
    std::vector<unsigned long> matching_stats_backward = {1,2,5,6,3,7,8,4,2,6,2,3,12,23,45,6};
    std::vector<bool> under_kmer(matching_stats_forward.size() , false);

    std::vector<bool> mat    (matching_stats_forward.size() , false);
    std::vector<bool> mat_rev(matching_stats_backward.size(), false);

    std::vector<bool> expected_mat     = {false,false,false,false,false,true,true,true,true,true,false,false,false,false,false,false};
    std::vector<bool> expected_mat_rev = {false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,false};
    std::vector<bool> expec_under_kmer = {false,false,true,true,true,true,true,true,true,true,true,true,false,false,false,false,};

    unsigned int kmer_length   = 5;
    unsigned int kmer_length_2 = 10;
    unsigned int probe_length  = 15;

    filter_helpers.rule_out_big_matches(matching_stats_forward, matching_stats_backward, under_kmer, kmer_length, kmer_length_2, probe_length, mat, mat_rev);

    for(auto count=0; count< matching_stats_backward.size(); ++count){
        ASSERT_EQ(expec_under_kmer.at(count), under_kmer.at(count));
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));

    }

}

TEST_F(SimpleFilterTest, ExcludeListFilterTest){

    std::string pattern("aaaaaaagcgcgcgcccgggccgggccggtttttttt");

    std::vector<bool> mat    (pattern.length(), false);
    std::vector<bool> mat_rev(pattern.length(), false);
    std::string exclude(pattern.length(), '1');


    std::vector<bool> expected_mat    (pattern.length(), true);
    std::vector<bool> expected_mat_rev(pattern.length(), true);

    filter_helpers.exclude_list_filter(13, exclude, mat, mat_rev);

    for(auto count=0; count< pattern.length(); ++count){
        ASSERT_EQ(expected_mat.at(count), mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));

    }

    mat     = std::vector<bool>(pattern.length(), false);
    mat_rev = std::vector<bool>(pattern.length(), false);

    expected_mat     = {false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true};
    expected_mat_rev = {false,false,false,false,false,false,false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true};

    exclude = std::string(pattern.length(), '0');

    filter_helpers.exclude_list_filter(25, exclude, mat, mat_rev);

    for(auto count=0; count< pattern.length(); ++count){
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));

    }

    mat     = std::vector<bool>(pattern.length(), false);
    mat_rev = std::vector<bool>(pattern.length(), false);

    expected_mat     = {false,false,false,true,false,true,false,false,false,true,true,false,false,true,false,false,true,false,false,false,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true};
    expected_mat_rev = {false,true,false,true,false,false,false,true,false,false,true,false,false,true,true,false,false,false,true,false,true,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true};

    exclude = std::string("0001010001100100100010101001001010000");

    filter_helpers.exclude_list_filter(14, exclude, mat, mat_rev);

    for(auto count=0; count< pattern.length(); ++count){
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));
    }

    mat     = std::vector<bool>(pattern.length(), false);
    mat_rev = std::vector<bool>(pattern.length(), false);

    expected_mat     = {true,true,true,true,true,true,true,true,false,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true};
    expected_mat_rev = {false,true,false,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true};

    exclude = std::string("1111111101010010010001001001101001000");

    filter_helpers.exclude_list_filter(25, exclude, mat, mat_rev);

    for(auto count=0; count< pattern.length(); ++count){
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));
    }
}

TEST_F(SimpleFilterTest, KmerCountFilterTest1){

    unsigned int kmer_length   = 5;
    unsigned int probe_length  = 9;

    std::vector<unsigned long> kmer_count = {1,1,1,1,1,157,158,159,160};

    std::vector<bool> expected_mat     = {false,true,true,true,true,true,true,true,true};
    std::vector<bool> expected_mat_rev = {false,true,true,true,true,true,true,true,true};


    std::vector<bool> mat     (kmer_count.size(), false);
    std::vector<bool> mat_rev (kmer_count.size(), false);

    filter_helpers.kmer_count_filter(probe_length, kmer_length, kmer_count, mat, mat_rev);

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));
    }

    kmer_length   = 4;
    probe_length  = 6;

    kmer_count = {300,300,300,300,300,300,300,300,300};

    expected_mat     = {false,false,false,false,true,true,true,true,true};
    expected_mat_rev = {false,false,false,false,true,true,true,true,true};

    mat     = std::vector<bool>(kmer_count.size(), false);
    mat_rev = std::vector<bool>(kmer_count.size(), false);

    filter_helpers.kmer_count_filter(probe_length, kmer_length, kmer_count, mat, mat_rev);

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));
    }

    kmer_length   = 2;
    probe_length  = 6;

    kmer_count = {300,300,300,300,300,300,300,300,300};

    expected_mat     = {true,true,true,true,true,true,true,true,true};
    expected_mat_rev = {true,true,true,true,true,true,true,true,true};

    mat     = std::vector<bool>(kmer_count.size(), false);
    mat_rev = std::vector<bool>(kmer_count.size(), false);

    filter_helpers.kmer_count_filter(probe_length, kmer_length, kmer_count, mat, mat_rev);

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));
    }
}

TEST_F(SimpleFilterTest, KmerCountFilterTest2){

    unsigned int kmer_length   = 2;
    unsigned int probe_length  = 6;

    std::vector<unsigned long> kmer_count = {171,171,171,171,171,171,171,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1,4,186};

    std::vector<bool> expected_mat     = {false,false,false,false,false,false,true,true,true,true,true,true,true,true,true,false,true,true,true,true,true};
    std::vector<bool> expected_mat_rev = {false,true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,true,true,true,true,true};


    std::vector<bool> mat     (kmer_count.size(), false);
    std::vector<bool> mat_rev (kmer_count.size(), false);

    filter_helpers.kmer_count_filter(probe_length, kmer_length, kmer_count, mat, mat_rev);

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(expected_mat.at(count),     mat.at(count));
        ASSERT_EQ(expected_mat_rev.at(count), mat_rev.at(count));
    }
}