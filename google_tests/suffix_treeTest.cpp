//
// Created by Odyss on 03/03/2020.
//

#include <gtest/gtest.h>
#include "../src/suffix_tree.h"
#include <gmock/gmock.h>

class BasicFunctionTest: public ::testing::Test {
public:
    BasicFunctionTest( ) {
        // initialization;
        // can also be done in SetUp()
    }

    suffix_tree test;
    suffix_tree test2;

    void SetUp( ) {
        test  = suffix_tree("/mnt/c/Users/Odyss/Documents/probe-design/test_data/expected_output/text.cst");
        test2 = suffix_tree("/mnt/c/Users/Odyss/Documents/probe-design/test_data/expected_output/text2.cst");

        std::cout << "TEST RUNNING: " << test.get_size() << std::endl;
        // initialization or some code to run before each test
    }

    void TearDown( ) {
        // code to run after each test;
        // can be used instead of a destructor,
        // but exceptions can be handled in this function only
    }

    ~BasicFunctionTest( )  {
        //resources cleanup, no exceptions allowed
    }

    // shared user data
};

/*int main( int argc, char *argv[] ) {
    ::testing::InitGoogleMock( &argc, argv );
    return RUN_ALL_TESTS( );
}*/

TEST_F (BasicFunctionTest /*test suite name*/, LoadTreeTest /*test name*/) {
    ASSERT_EQ (test.get_size(), 341);     // success
    ASSERT_EQ (test2.get_size(), 295);     // success

}

TEST_F (BasicFunctionTest /*test suite name*/, MatchingStatsTest1 /*test name*/) {

    std::vector<unsigned long> matching_stats;
    std::vector<unsigned long> marked_nodes;

    std::tie(matching_stats, marked_nodes) = test.matching_statistics("aacc");
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected = {4,3,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test.matching_statistics("gacc");

    std::vector<unsigned long> expected1 = {0,3,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected1.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test.matching_statistics("aaccgccaa");

    std::vector<unsigned long> expected2 = {4,3,2,1,0,4,3,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected2.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test.matching_statistics("ggggtttttggggg");

    std::vector<unsigned long> expected3 = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected3.at(count));
    }
}

TEST_F (BasicFunctionTest /*test suite name*/, MatchingStatsTest2 /*test name*/) {

    std::vector<unsigned long> matching_stats;
    std::vector<unsigned long> marked_nodes;

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics("aacc");
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected = {3,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics("gacc");

    std::vector<unsigned long> expected1 = {2,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected1.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics("aaccgccaa");

    std::vector<unsigned long> expected2 = {3,2,3,2,3,4,3,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected2.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics("ggggtttttggggg");

    std::vector<unsigned long> expected3 = {2,2,2,3,3,3,3,3,2,2,2,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected3.at(count));
    }
}

TEST_F (BasicFunctionTest /*test suite name*/, MatchingStatsTest3 /*test name*/) {

    std::vector<unsigned long> matching_stats;
    std::vector<unsigned long> marked_nodes;

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics_mark_depth("ttgc",0);

    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected0 = {0,0,0,0};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected0.at(count));

    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics_mark_depth("ttgc",1);
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected1 = {1,1,1,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected1.at(count));

    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics_mark_depth("ttgc",2);
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected2 = {2,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){

        ASSERT_EQ(matching_stats.at(count), expected2.at(count));

    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics_mark_depth("ttgc",3);
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected3 = {2,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected3.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics_mark_depth("ttgc",4);
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected4 = {2,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected4.at(count));
    }

}

TEST_F (BasicFunctionTest, MatchingStatsTest4 ) {


    std::vector<unsigned long> matching_stats;
    std::vector<unsigned long> marked_nodes;

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics_mark_depth("cccc",2);
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected1 = {2,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected1.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test.matching_statistics_mark_depth("cccccccccccccccccccccccccccccccccccccccc",30);
    ASSERT_EQ (matching_stats.size(), 40);
    ASSERT_EQ (marked_nodes.size(), 40);

    std::vector<unsigned long> expected2 = {30,30,30,30,30,30,30,30,30,30,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected2.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.matching_statistics_mark_depth("cgtcgttgcatccgtatctctgattctattt",16);
    ASSERT_EQ (matching_stats.size(), 31);
    ASSERT_EQ (marked_nodes.size(), 31);

    std::vector<unsigned long> expected = {3,2,2,3,2,2,2,2,3,3,2,2,3,2,3,3,3,2,3,2,2,1,4,3,3,2,4,3,2,2,1};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected.at(count));
    }
}

TEST_F (BasicFunctionTest, KmerCountTest1) {

    std::vector<unsigned long> kmer_count = test.kmer_count("aaaaacccc", 9);

    std::vector<unsigned long> expected = {1,1,1,1,1,157,158,159,160};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }

    kmer_count = test.kmer_count("aaaaacccc", 4);

    expected = {173,173,1,1,1,157,158,159,160};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }

    kmer_count = test.kmer_count("aaaaacccc", 0);

    expected = {0,0,0,0,0,0,0,0,0};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }

    kmer_count = test.kmer_count("aaaaacccc", 1);

    expected = {179,179,179,179,179,160,160,160,160};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }
}

TEST_F (BasicFunctionTest, KmerCountTest2) {

    std::vector<unsigned long> kmer_count = test2.kmer_count("catattcaggcccccccccc", 9);

    std::vector<unsigned long> expected = {1,1,1,1,1,1,1,1,1,1,65,65,66,67,68,69,70,71,73,83};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }

    kmer_count = test2.kmer_count("aaaaaaaaaaacgtcgttgca", 5);

    expected = {171,171,171,171,171,171,171,1,1,1,1,1,1,1,1,1,1,1,1,4,186};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }

    kmer_count = test2.kmer_count("ccccaaaaaaaaaaa", 16);

    expected = {1,1,1,1,159,161,163,165,167,169,171,173,175,177,186};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }

    kmer_count = test2.kmer_count("ccccccccccccccccccccccccccccccccccccccccccccc", 13);

    expected = {61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,61,62,63,64,65,66,67,68,69,70,71,73,83};

    for(auto count=0; count< kmer_count.size(); ++count){
        ASSERT_EQ(kmer_count.at(count), expected.at(count));
    }
}