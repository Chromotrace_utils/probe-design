project(google_tests)

# 'lib' is the folder with Google Test sources
add_subdirectory(lib)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})


find_library(SDSL_LIB sdsl
        HINTS ${PROJECT_SOURCE_DIR}/../external/lib/)
find_path(SDSL_INC sdsl
        HINTS ${PROJECT_SOURCE_DIR}/../external/include/)
message(STATUS ${SDSL_LIB})

find_library(DIV_SUF_LIB divsufsort
        HINTS ${PROJECT_SOURCE_DIR}/../external/lib/)

message(STATUS ${DIV_SUF_LIB})

find_library(DIV_SUF64 divsufsort64
        HINTS ${PROJECT_SOURCE_DIR}/../external/lib/)
message(STATUS ${DIV_SUF64})


# 'Google_Tests_run' is the target name
# 'test1.cpp tests2.cpp' are source files with tests
#This fails on build on gitlab. Think when I build on my laptop it links againt a local install, not the embedded one. This doesn't happen on gitlab.
add_executable(Google_Tests_run suffix_treeTest.cpp ../src/suffix_tree.h ../src/suffix_tree.cpp read_fasta_filesTest.cpp ../src/read_fasta_files.h ../src/read_fasta_files.cpp simple_filtersTest.cpp ../src/simple_filters.h ../src/simple_filters.cpp)
target_link_libraries(Google_Tests_run  ${SDSL_LIB} ${DIV_SUF_LIB} ${DIV_SUF64} gtest gtest_main)
target_include_directories(Google_Tests_run PUBLIC ${SDSL_INC})

