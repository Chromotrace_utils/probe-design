# Google Tests README

Unit tests for the probe_design can be found in this folder. Currently the locations of the files in the test_data file are hardcoded into the test.
These should be changed to an absolute file path on your local system before the tests are run.

To run the tests build the main project and then run the resulting binary from the /google_tests/build directory. If you wish to build this seperately you should run the following commands.

```
mkdir build
cd build
cmake ..
make
```
